

function Potree(){
	
}

Potree.pointLoadLimit = 50*1000*1000;
Potree.version = {
	major: 1,
	minor: 5,
	suffix: "RC"
};

console.log("Potree " + Potree.version.major + "." + Potree.version.minor + Potree.version.suffix);


Potree.Version = function(version){
	this.version = version;
	var vmLength = (version.indexOf(".") === -1) ? version.length : version.indexOf(".");
	this.versionMajor = parseInt(version.substr(0, vmLength));
	this.versionMinor = parseInt(version.substr(vmLength + 1));
	if(this.versionMinor.length === 0){
		this.versionMinor = 0;
	}

};

Potree.Version.prototype.newerThan = function(version){
	var v = new Potree.Version(version);

	if( this.versionMajor > v.versionMajor){
		return true;
	}else if( this.versionMajor === v.versionMajor && this.versionMinor > v.versionMinor){
		return true;
	}else{
		return false;
	}
};

Potree.Version.prototype.equalOrHigher = function(version){
	var v = new Potree.Version(version);

	if( this.versionMajor > v.versionMajor){
		return true;
	}else if( this.versionMajor === v.versionMajor && this.versionMinor >= v.versionMinor){
		return true;
	}else{
		return false;
	}
};

Potree.Version.prototype.upTo = function(version){
	return !this.newerThan(version);
};

Potree.PointAttributeNames = {};

Potree.PointAttributeNames.POSITION_CARTESIAN 	= 0;	// float x, y, z;
Potree.PointAttributeNames.COLOR_PACKED		= 1;	// byte r, g, b, a; 	I = [0,1]
Potree.PointAttributeNames.COLOR_FLOATS_1		= 2;	// float r, g, b; 		I = [0,1]
Potree.PointAttributeNames.COLOR_FLOATS_255	= 3;	// float r, g, b; 		I = [0,255]
Potree.PointAttributeNames.NORMAL_FLOATS		= 4;  	// float x, y, z;
Potree.PointAttributeNames.FILLER				= 5;
Potree.PointAttributeNames.INTENSITY			= 6;
Potree.PointAttributeNames.CLASSIFICATION		= 7;
Potree.PointAttributeNames.NORMAL_SPHEREMAPPED	= 8;
Potree.PointAttributeNames.NORMAL_OCT16		= 9;
Potree.PointAttributeNames.NORMAL				= 10;


Potree.PointAttribute = function(name, type, numElements){
	this.name = name;
	this.type = type;
	this.numElements = numElements;
	this.byteSize = this.numElements * this.type.size;
};

Potree.PointAttributeTypes = {
	DATA_TYPE_DOUBLE	: {ordinal : 0, size: 8},
	DATA_TYPE_FLOAT		: {ordinal : 1, size: 4},
	DATA_TYPE_INT8		: {ordinal : 2, size: 1},
	DATA_TYPE_UINT8		: {ordinal : 3, size: 1},
	DATA_TYPE_INT16		: {ordinal : 4, size: 2},
	DATA_TYPE_UINT16	: {ordinal : 5, size: 2},
	DATA_TYPE_INT32		: {ordinal : 6, size: 4},
	DATA_TYPE_UINT32	: {ordinal : 7, size: 4},
	DATA_TYPE_INT64		: {ordinal : 8, size: 8},
	DATA_TYPE_UINT64	: {ordinal : 9, size: 8}
};

var i = 0;
for(var obj in Potree.PointAttributeTypes){
	Potree.PointAttributeTypes[i] = Potree.PointAttributeTypes[obj];
	i++;
}
		


Potree.PointAttribute.POSITION_CARTESIAN = new Potree.PointAttribute(
		Potree.PointAttributeNames.POSITION_CARTESIAN,
		Potree.PointAttributeTypes.DATA_TYPE_FLOAT, 3);

Potree.PointAttribute.RGBA_PACKED = new Potree.PointAttribute(
		Potree.PointAttributeNames.COLOR_PACKED,
		Potree.PointAttributeTypes.DATA_TYPE_INT8, 4);

Potree.PointAttribute.COLOR_PACKED = Potree.PointAttribute.RGBA_PACKED;

Potree.PointAttribute.RGB_PACKED = new Potree.PointAttribute(
		Potree.PointAttributeNames.COLOR_PACKED,
		Potree.PointAttributeTypes.DATA_TYPE_INT8, 3);

Potree.PointAttribute.NORMAL_FLOATS = new Potree.PointAttribute(
		Potree.PointAttributeNames.NORMAL_FLOATS,
		Potree.PointAttributeTypes.DATA_TYPE_FLOAT, 3);

Potree.PointAttribute.FILLER_1B = new Potree.PointAttribute(
		Potree.PointAttributeNames.FILLER,
		Potree.PointAttributeTypes.DATA_TYPE_UINT8, 1);

Potree.PointAttribute.INTENSITY = new Potree.PointAttribute(
		Potree.PointAttributeNames.INTENSITY,
		Potree.PointAttributeTypes.DATA_TYPE_UINT16, 1);

Potree.PointAttribute.CLASSIFICATION = new Potree.PointAttribute(
		Potree.PointAttributeNames.CLASSIFICATION,
		Potree.PointAttributeTypes.DATA_TYPE_UINT8, 1);

Potree.PointAttribute.NORMAL_SPHEREMAPPED = new Potree.PointAttribute(
		Potree.PointAttributeNames.NORMAL_SPHEREMAPPED,
		Potree.PointAttributeTypes.DATA_TYPE_UINT8, 2);

Potree.PointAttribute.NORMAL_OCT16 = new Potree.PointAttribute(
		Potree.PointAttributeNames.NORMAL_OCT16,
		Potree.PointAttributeTypes.DATA_TYPE_UINT8, 2);

Potree.PointAttribute.NORMAL = new Potree.PointAttribute(
		Potree.PointAttributeNames.NORMAL,
		Potree.PointAttributeTypes.DATA_TYPE_FLOAT, 3);





Potree.PointAttributes = function(pointAttributes){
	this.attributes = [];
	this.byteSize = 0;
	this.size = 0;

	if(pointAttributes != null){
		for(var i = 0; i < pointAttributes.length; i++){
			var pointAttributeName = pointAttributes[i];
			var pointAttribute = Potree.PointAttribute[pointAttributeName];
			this.attributes.push(pointAttribute);
			this.byteSize += pointAttribute.byteSize;
			this.size++;
		}
	}
};

Potree.PointAttributes.prototype.add = function(pointAttribute){
	this.attributes.push(pointAttribute);
	this.byteSize += pointAttribute.byteSize;
	this.size++;
};

Potree.PointAttributes.prototype.hasColors = function(){
	for(var name in this.attributes){
		var pointAttribute = this.attributes[name];
		if(pointAttribute.name === Potree.PointAttributeNames.COLOR_PACKED){
			return true;
		}
	}

	return false;
};

Potree.PointAttributes.prototype.hasNormals = function(){
	for(var name in this.attributes){
		var pointAttribute = this.attributes[name];
		if(
			pointAttribute === Potree.PointAttribute.NORMAL_SPHEREMAPPED ||
			pointAttribute === Potree.PointAttribute.NORMAL_FLOATS ||
			pointAttribute === Potree.PointAttribute.NORMAL ||
			pointAttribute === Potree.PointAttribute.NORMAL_OCT16){
			return true;
		}
	}

	return false;
};



Potree.utils = class{

	static loadShapefileFeatures(file, callback){

		let features = [];

		let handleFinish = () => {
			callback(features);
		};

		shapefile.open(file)
		.then(source => {source.read()
			.then(function log(result){
				if(result.done){
					handleFinish();
					return;
				}

				//console.log(result.value);

				if(result.value && result.value.type === "Feature" && result.value.geometry !== undefined){
					features.push(result.value);
				}

				return source.read().then(log);
			})
		});
	}

	static toString(value){
		if(value instanceof THREE.Vector3){
			return value.x.toFixed(2) + ", " + value.y.toFixed(2) + ", " + value.z.toFixed(2);
		}else{
			return "" + value + "";
		}
	}

	static normalizeURL(url){
		let u = new URL(url);

		return u.protocol + "//" + u.hostname + u.pathname.replace(/\/+/g, "/");
	};

	static pathExists(url){
		let req = new XMLHttpRequest();
		req.open('GET', url, false);
		req.send(null);
		if (req.status !== 200) {
			return false;
		}
		return true;
	};

	/**
	 * adapted from mhluska at https://github.com/mrdoob/three.js/issues/1561
	 */
	static computeTransformedBoundingBox(box, transform){

		let vertices = [
			new THREE.Vector3(box.min.x, box.min.y, box.min.z).applyMatrix4(transform),
			new THREE.Vector3(box.min.x, box.min.y, box.min.z).applyMatrix4(transform),
			new THREE.Vector3(box.max.x, box.min.y, box.min.z).applyMatrix4(transform),
			new THREE.Vector3(box.min.x, box.max.y, box.min.z).applyMatrix4(transform),
			new THREE.Vector3(box.min.x, box.min.y, box.max.z).applyMatrix4(transform),
			new THREE.Vector3(box.min.x, box.max.y, box.max.z).applyMatrix4(transform),
			new THREE.Vector3(box.max.x, box.max.y, box.min.z).applyMatrix4(transform),
			new THREE.Vector3(box.max.x, box.min.y, box.max.z).applyMatrix4(transform),
			new THREE.Vector3(box.max.x, box.max.y, box.max.z).applyMatrix4(transform)
		];

		let boundingBox = new THREE.Box3();
		boundingBox.setFromPoints( vertices );

		return boundingBox;
	};

	/**
	 * add separators to large numbers
	 *
	 * @param nStr
	 * @returns
	 */
	static addCommas(nStr){
		nStr += '';
		let x = nStr.split('.');
		let x1 = x[0];
		let x2 = x.length > 1 ? '.' + x[1] : '';
		let rgx = /(\d+)(\d{3})/;
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		}
		return x1 + x2;
	};

	static removeCommas(str){
		return str.replace(/,/g, "");
	}

	/**
	 * create worker from a string
	 *
	 * code from http://stackoverflow.com/questions/10343913/how-to-create-a-web-worker-from-a-string
	 */
	static createWorker(code){
		 let blob = new Blob([code], {type: 'application/javascript'});
		 let worker = new Worker(URL.createObjectURL(blob));

		 return worker;
	};

	static loadSkybox(path){
		let camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 100000 );
		camera.up.set(0, 0, 1);
		let scene = new THREE.Scene();

		let format = ".jpg";
		let urls = [
			path + 'px' + format, path + 'nx' + format,
			path + 'py' + format, path + 'ny' + format,
			path + 'pz' + format, path + 'nz' + format
		];

		//var materialArray = [];
		//for (var i = 0; i < 6; i++){
		//	materialArray.push( new THREE.MeshBasicMaterial({
		//		map: THREE.ImageUtils.loadTexture( urls[i] ),
		//		side: THREE.BackSide,
		//		depthTest: false,
		//		depthWrite: false
		//		})
		//	);
		//}

		let materialArray = [];
		{
			for (let i = 0; i < 6; i++){

				let material = new THREE.MeshBasicMaterial({
					map: null,
					side: THREE.BackSide,
					depthTest: false,
					depthWrite: false
				});

				materialArray.push(material);

				let loader = new THREE.TextureLoader();
				loader.load( urls[i],
					function loaded(texture){
						material.map = texture;
						material.needsUpdate = true;
					},function progress(xhr){
						//console.log( (xhr.loaded / xhr.total * 100) + '% loaded' );
					},function error(xhr){
						console.log( 'An error happened', xhr );
					}
				);
			}


		}

		var skyGeometry = new THREE.CubeGeometry( 5000, 5000, 5000 );
		var skybox = new THREE.Mesh( skyGeometry, materialArray );

		scene.add(skybox);

		// z up
		scene.rotation.x = Math.PI / 2;

		return {"camera": camera, "scene": scene};
	};

	static createGrid(width, length, spacing, color){
		let material = new THREE.LineBasicMaterial({
			color: color || 0x888888
		});

		let geometry = new THREE.Geometry();
		for(let i = 0; i <= length; i++){
			 geometry.vertices.push(new THREE.Vector3(-(spacing*width)/2, i*spacing-(spacing*length)/2, 0));
			 geometry.vertices.push(new THREE.Vector3(+(spacing*width)/2, i*spacing-(spacing*length)/2, 0));
		}

		for(let i = 0; i <= width; i++){
			 geometry.vertices.push(new THREE.Vector3(i*spacing-(spacing*width)/2, -(spacing*length)/2, 0));
			 geometry.vertices.push(new THREE.Vector3(i*spacing-(spacing*width)/2, +(spacing*length)/2, 0));
		}

		let line = new THREE.LineSegments(geometry, material, THREE.LinePieces);
		line.receiveShadow = true;
		return line;
	};


	static createBackgroundTexture(width, height){

		function gauss(x, y){
			return (1 / (2 * Math.PI)) * Math.exp( - (x*x + y*y) / 2);
		};

		//map.magFilter = THREE.NearestFilter;
		let size = width * height;
		let data = new Uint8Array( 3 * size );

		let chroma = [1, 1.5, 1.7];
		let max = gauss(0, 0);

		for(let x = 0; x < width; x++){
			for(let y = 0; y < height; y++){
				let u = 2 * (x / width) - 1;
				let v = 2 * (y / height) - 1;

				let i = x + width*y;
				let d = gauss(2*u, 2*v) / max;
				let r = (Math.random() + Math.random() + Math.random()) / 3;
				r = (d * 0.5 + 0.5) * r * 0.03;
				r = r * 0.4;

				//d = Math.pow(d, 0.6);

				data[3*i+0] = 255 * (d / 15 + 0.05 + r) * chroma[0];
				data[3*i+1] = 255 * (d / 15 + 0.05 + r) * chroma[1];
				data[3*i+2] = 255 * (d / 15 + 0.05 + r) * chroma[2];
			}
		}

		let texture = new THREE.DataTexture( data, width, height, THREE.RGBFormat );
		texture.needsUpdate = true;

		return texture;
	};

	static getMousePointCloudIntersection(mouse, camera, renderer, pointclouds){
		let nmouse =  {
			x: (mouse.x / renderer.domElement.clientWidth ) * 2 - 1,
			y: - (mouse.y / renderer.domElement.clientHeight ) * 2 + 1
		};

		//let vector = new THREE.Vector3( nmouse.x, nmouse.y, 0.5 );
		//vector.unproject(camera);

		//let direction = vector.sub(camera.position).normalize();
		//let ray = new THREE.Ray(camera.position, direction);

		let raycaster = new THREE.Raycaster();
		raycaster.setFromCamera(nmouse, camera);
		let ray = raycaster.ray;

		let selectedPointcloud = null;
		let closestDistance = Infinity;
		let closestIntersection = null;
		let closestPoint = null;

		for(let pointcloud of pointclouds){
			let point = pointcloud.pick(renderer, camera, ray);

			if(!point){
				continue;
			}

			let distance = camera.position.distanceTo(point.position);

			if(distance < closestDistance){
				closestDistance = distance;
				selectedPointcloud = pointcloud;
				closestIntersection = point.position;
				closestPoint = point;
			}
		}

		if(selectedPointcloud){
			return {
				location: closestIntersection,
				distance: closestDistance,
				pointcloud: selectedPointcloud,
				point: closestPoint
			};
		}else{
			return null;
		}
	};

	static pixelsArrayToImage(pixels, width, height){
		let canvas = document.createElement('canvas');
		canvas.width = width;
		canvas.height = height;

		let context = canvas.getContext('2d');

		pixels = new pixels.constructor(pixels);

		for(let i = 0; i < pixels.length; i++){
			pixels[i*4 + 3] = 255;
		}

		let imageData = context.createImageData(width, height);
		imageData.data.set(pixels);
		context.putImageData(imageData, 0, 0);

		let img = new Image();
		img.src = canvas.toDataURL();
		//img.style.transform = "scaleY(-1)";

		return img;
	};

	static projectedRadius(radius, fov, distance, screenHeight){
		let projFactor =  (1 / Math.tan(fov / 2)) / distance;
		projFactor = projFactor * screenHeight / 2;

		return radius * projFactor;
	};


	static topView(camera, node){
		camera.position.set(0, 1, 0);
		camera.rotation.set(-Math.PI / 2, 0, 0);
		camera.zoomTo(node, 1);
	};

	static frontView(camera, node){
		camera.position.set(0, 0, 1);
		camera.rotation.set(0, 0, 0);
		camera.zoomTo(node, 1);
	};


	static leftView(camera, node){
		camera.position.set(-1, 0, 0);
		camera.rotation.set(0, -Math.PI / 2, 0);
		camera.zoomTo(node, 1);
	};

	static rightView(camera, node){
		camera.position.set(1, 0, 0);
		camera.rotation.set(0, Math.PI / 2, 0);
		camera.zoomTo(node, 1);
	};

	/**
	 *
	 * 0: no intersection
	 * 1: intersection
	 * 2: fully inside
	 */
	static frustumSphereIntersection(frustum, sphere){
		let planes = frustum.planes;
		let center = sphere.center;
		let negRadius = - sphere.radius;

		let minDistance = Number.MAX_VALUE;

		for ( let i = 0; i < 6; i ++ ) {

			let distance = planes[ i ].distanceToPoint( center );

			if ( distance < negRadius ) {

				return 0;

			}

			minDistance = Math.min(minDistance, distance);

		}

		return (minDistance >= sphere.radius) ? 2 : 1;
	};

	// code taken from three.js
	// ImageUtils - generateDataTexture()
	static generateDataTexture(width, height, color){
		let size = width * height;
		let data = new Uint8Array(3 * width * height);

		let r = Math.floor( color.r * 255 );
		let g = Math.floor( color.g * 255 );
		let b = Math.floor( color.b * 255 );

		for ( let i = 0; i < size; i ++ ) {
			data[ i * 3 ] 	   = r;
			data[ i * 3 + 1 ] = g;
			data[ i * 3 + 2 ] = b;
		}

		let texture = new THREE.DataTexture( data, width, height, THREE.RGBFormat );
		texture.needsUpdate = true;
		texture.magFilter = THREE.NearestFilter;

		return texture;
	};

	// from http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
	static getParameterByName(name) {
		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		let regex = new RegExp("[\\?&]" + name + "=([^&#]*)"), results = regex.exec(location.search);
		return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
	}

	static setParameter(name, value){
		//value = encodeURIComponent(value);

		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		let regex = new RegExp("([\\?&])(" + name + "=([^&#]*))");
		let results = regex.exec(location.search);

		let url = window.location.href;
		if(results === null){
			if(window.location.search.length === 0){
				url = url + "?";
			}else{
				url = url + "&";
			}

			url = url + name + "=" + value;
		}else{
			let newValue = name + "=" + value;
			url = url.replace(results[2], newValue);
		}
		window.history.replaceState({}, "", url);
	}

};

Potree.utils.screenPass = new function(){

	this.screenScene = new THREE.Scene();
	this.screenQuad = new THREE.Mesh(new THREE.PlaneBufferGeometry(2, 2, 0));
	this.screenQuad.material.depthTest = true;
	this.screenQuad.material.depthWrite = true;
	this.screenQuad.material.transparent = true;
	this.screenScene.add(this.screenQuad);
	this.camera = new THREE.Camera();

	this.render = function(renderer, material, target){
		this.screenQuad.material = material;

		if(typeof target === "undefined"){
			renderer.render(this.screenScene, this.camera);
		}else{
			renderer.render(this.screenScene, this.camera, target);
		}
	};
}();

Potree.utils.prototype.generateDataTexture = function(width, height, color){
	let size = width * height;
	let data = new Uint8Array(3 * width * height);

	let r = Math.floor( color.r * 255 );
	let g = Math.floor( color.g * 255 );
	let b = Math.floor( color.b * 255 );

	for ( let i = 0; i < size; i ++ ) {
		data[ i * 3 ] 	   = r;
		data[ i * 3 + 1 ] = g;
		data[ i * 3 + 2 ] = b;
	}

	let texture = new THREE.DataTexture( data, width, height, THREE.RGBFormat );
	texture.needsUpdate = true;
	texture.magFilter = THREE.NearestFilter;

	return texture;
};


Potree.BinaryLoader = function(version, boundingBox, scale){
	if(typeof(version) === "string"){
		this.version = new Potree.Version(version);
	}else{
		this.version = version;
	}

	this.boundingBox = boundingBox;
	this.scale = scale;
};

Potree.BinaryLoader.prototype.load = function(node){
	if(node.loaded){
		return;
	}

	let scope = this;

	let url = node.getURL();

	if(this.version.equalOrHigher("1.4")){
		url += ".bin";
	}

	let xhr = new XMLHttpRequest();
	xhr.open('GET', url, true);
	xhr.responseType = 'arraybuffer';
	xhr.overrideMimeType('text/plain; charset=x-user-defined');
	xhr.onreadystatechange = function() {
		if (xhr.readyState === 4) {
			if (xhr.status === 200 || xhr.status === 0) {
				let buffer = xhr.response;
				scope.parse(node, buffer);
			} else {
				console.log('Failed to load file! HTTP status: ' + xhr.status + ", file: " + url);
			}
		}
	};
	try{
		xhr.send(null);
	}catch(e){
		console.log("fehler beim laden der punktwolke: " + e);
	}
};

Potree.BinaryLoader.prototype.parse = function(node, buffer){

	let numPoints = buffer.byteLength / node.pcoGeometry.pointAttributes.byteSize;
	let pointAttributes = node.pcoGeometry.pointAttributes;

	if(this.version.upTo("1.5")){
		node.numPoints = numPoints;
	}

	let workerPath = Potree.scriptPath + "/workers/BinaryDecoderWorker.js";
	let worker = Potree.workerPool.getWorker(workerPath);

	worker.onmessage = function(e){
		let data = e.data;
		let buffers = data.attributeBuffers;
		let tightBoundingBox = new THREE.Box3(
			new THREE.Vector3().fromArray(data.tightBoundingBox.min),
			new THREE.Vector3().fromArray(data.tightBoundingBox.max)
		);

		Potree.workerPool.returnWorker(workerPath, worker);

		let geometry = new THREE.BufferGeometry();

		for(let property in buffers){
			if(buffers.hasOwnProperty(property)){
				let buffer = buffers[property].buffer;
				let attribute = buffers[property].attribute;
				let numElements = attribute.numElements;

				if(parseInt(property) === Potree.PointAttributeNames.POSITION_CARTESIAN){
					geometry.addAttribute("position", new THREE.BufferAttribute(new Float32Array(buffer), 3));
				}else if(parseInt(property) === Potree.PointAttributeNames.COLOR_PACKED){
					geometry.addAttribute("color", new THREE.BufferAttribute(new Uint8Array(buffer), 3, true));
				}else if(parseInt(property) === Potree.PointAttributeNames.INTENSITY){
					geometry.addAttribute("intensity", new THREE.BufferAttribute(new Float32Array(buffer), 1));
				}else if(parseInt(property) === Potree.PointAttributeNames.CLASSIFICATION){
					geometry.addAttribute("classification", new THREE.BufferAttribute(new Uint8Array(buffer), 1));
				}else if(parseInt(property) === Potree.PointAttributeNames.NORMAL_SPHEREMAPPED){
					geometry.addAttribute("normal", new THREE.BufferAttribute(new Float32Array(buffer), 3));
				}else if(parseInt(property) === Potree.PointAttributeNames.NORMAL_OCT16){
					geometry.addAttribute("normal", new THREE.BufferAttribute(new Float32Array(buffer), 3));
				}else if(parseInt(property) === Potree.PointAttributeNames.NORMAL){
					geometry.addAttribute("normal", new THREE.BufferAttribute(new Float32Array(buffer), 3));
				}
			}
		}
		let indicesAttribute = new THREE.Uint8BufferAttribute(data.indices, 4);
		indicesAttribute.normalized = true;
		geometry.addAttribute("indices", indicesAttribute);

		if(!geometry.attributes.normal){
			let buffer = new Float32Array(numPoints*3);
			geometry.addAttribute("normal", new THREE.BufferAttribute(new Float32Array(buffer), 3));
		}

		tightBoundingBox.max.sub(tightBoundingBox.min);
		tightBoundingBox.min.set(0, 0, 0);

		geometry.boundingBox = node.boundingBox;
		node.geometry = geometry;
		node.mean = new THREE.Vector3(...data.mean);
		node.tightBoundingBox = tightBoundingBox;
		node.loaded = true;
		node.loading = false;
		node.pcoGeometry.numNodesLoading--;
	};

	let message = {
		buffer: buffer,
		pointAttributes: pointAttributes,
		version: this.version.version,
		min: [ node.boundingBox.min.x, node.boundingBox.min.y, node.boundingBox.min.z ],
		offset: [node.pcoGeometry.offset.x, node.pcoGeometry.offset.y, node.pcoGeometry.offset.z],
		scale: this.scale
	};
	worker.postMessage(message, [message.buffer]);

};

Potree.POCLoader = function(){

};


/**
 * @return a point cloud octree with the root node data loaded.
 * loading of descendants happens asynchronously when they're needed
 *
 * @param url
 * @param loadingFinishedListener executed after loading the binary has been finished
 */
Potree.POCLoader.load = function load(url, callback) {
	try{
		let pco = new Potree.PointCloudOctreeGeometry();
		pco.url = url;
		let xhr = new XMLHttpRequest();
		xhr.open('GET', url, true);

		xhr.onreadystatechange = function(){
			if(xhr.readyState === 4 && (xhr.status === 200 || xhr.status === 0)){
				let fMno = JSON.parse(xhr.responseText);

				let version = new Potree.Version(fMno.version);

				// assume octreeDir is absolute if it starts with http
				if(fMno.octreeDir.indexOf("http") === 0){
					pco.octreeDir = fMno.octreeDir;
				}else{
					pco.octreeDir = url + "/../" + fMno.octreeDir;
				}

				pco.spacing = fMno.spacing;
				pco.hierarchyStepSize = fMno.hierarchyStepSize;

				pco.pointAttributes = fMno.pointAttributes;

				let min = new THREE.Vector3(fMno.boundingBox.lx, fMno.boundingBox.ly, fMno.boundingBox.lz);
				let max = new THREE.Vector3(fMno.boundingBox.ux, fMno.boundingBox.uy, fMno.boundingBox.uz);
				let boundingBox = new THREE.Box3(min, max);
				let tightBoundingBox = boundingBox.clone();

				if(fMno.tightBoundingBox){
					tightBoundingBox.min.copy(new THREE.Vector3(fMno.tightBoundingBox.lx, fMno.tightBoundingBox.ly, fMno.tightBoundingBox.lz));
					tightBoundingBox.max.copy(new THREE.Vector3(fMno.tightBoundingBox.ux, fMno.tightBoundingBox.uy, fMno.tightBoundingBox.uz));
				}

				let offset = min.clone();

				boundingBox.min.sub(offset);
				boundingBox.max.sub(offset);

				tightBoundingBox.min.sub(offset);
				tightBoundingBox.max.sub(offset);

				pco.projection = fMno.projection;
				pco.boundingBox = boundingBox;
				pco.tightBoundingBox = tightBoundingBox;
				pco.boundingSphere = boundingBox.getBoundingSphere();
				pco.tightBoundingSphere = tightBoundingBox.getBoundingSphere();
				pco.offset = offset;
				if(fMno.pointAttributes === "LAS"){
					pco.loader = new Potree.LasLazLoader(fMno.version);
				}else if(fMno.pointAttributes === "LAZ"){
					pco.loader = new Potree.LasLazLoader(fMno.version);
				}else{
					pco.loader = new Potree.BinaryLoader(fMno.version, boundingBox, fMno.scale);
					pco.pointAttributes = new Potree.PointAttributes(pco.pointAttributes);
				}

				let nodes = {};

				{ // load root
					let name = "r";

					let root = new Potree.PointCloudOctreeGeometryNode(name, pco, boundingBox);
					root.level = 0;
					root.hasChildren = true;
					root.spacing = pco.spacing;
					if(version.upTo("1.5")){
						root.numPoints = fMno.hierarchy[0][1];
					}else{
						root.numPoints = 0;
					}
					pco.root = root;
					pco.root.load();
					nodes[name] = root;
				}

				// load remaining hierarchy
				if(version.upTo("1.4")){
					for( let i = 1; i < fMno.hierarchy.length; i++){
						let name = fMno.hierarchy[i][0];
						let numPoints = fMno.hierarchy[i][1];
						let index = parseInt(name.charAt(name.length-1));
						let parentName = name.substring(0, name.length-1);
						let parentNode = nodes[parentName];
						let level = name.length-1;
						let boundingBox = Potree.POCLoader.createChildAABB(parentNode.boundingBox, index);

						let node = new Potree.PointCloudOctreeGeometryNode(name, pco, boundingBox);
						node.level = level;
						node.numPoints = numPoints;
						node.spacing = pco.spacing / Math.pow(2, level);
						parentNode.addChild(node);
						nodes[name] = node;
					}
				}

				pco.nodes = nodes;

				callback(pco);
			}
		};

		xhr.send(null);
	}catch(e){
		console.log("loading failed: '" + url + "'");
		console.log(e);

		callback();
	}
};

Potree.POCLoader.loadPointAttributes = function(mno){

	let fpa = mno.pointAttributes;
	let pa = new Potree.PointAttributes();

	for(let i = 0; i < fpa.length; i++){
		let pointAttribute = Potree.PointAttribute[fpa[i]];
		pa.add(pointAttribute);
	}

	return pa;
};


Potree.POCLoader.createChildAABB = function(aabb, index){

	let min = aabb.min.clone();
	let max = aabb.max.clone();
	let size = new THREE.Vector3().subVectors(max, min);

	if((index & 0b0001) > 0){
		min.z += size.z / 2;
	}else{
		max.z -= size.z / 2;
	}

	if((index & 0b0010) > 0){
		min.y += size.y / 2;
	}else{
		max.y -= size.y / 2;
	}

	if((index & 0b0100) > 0){
		min.x += size.x / 2;
	}else{
		max.x -= size.x / 2;
	}

	return new THREE.Box3(min, max);
};


Potree.recalculate_area = function(){
	console.log('Recalculate');
	let measurements = measurements;
	for(let measure of measurements){
		console.log('Mesh',measurements);
	}

}
Potree.loadPointCloud = function(path, name, callback){

	let loaded = function(pointcloud){
		pointcloud.name = name;

		callback({type: "pointcloud_loaded", pointcloud: pointcloud});
	};

	// load pointcloud
	if(!path){

	}else if(path.indexOf("greyhound://") === 0){
		// We check if the path string starts with 'greyhound:', if so we assume it's a greyhound server URL.
		Potree.GreyhoundLoader.load(path, function(geometry) {
			if(!geometry){
				callback({type: "loading_failed"});
			}else{
				let pointcloud = new Potree.PointCloudOctree(geometry);
				loaded(pointcloud);
			}
		});
	}else if(path.indexOf("cloud.js") > 0){
		Potree.POCLoader.load(path, function(geometry){
			if(!geometry){
				callback({type: "loading_failed"});
			}else{
				let pointcloud = new Potree.PointCloudOctree(geometry);
				loaded(pointcloud);
			}
		}.bind(this));
	}else if(path.indexOf(".vpc") > 0){
		Potree.PointCloudArena4DGeometry.load(path, function(geometry){
			if(!geometry){
				callback({type: "loading_failed"});
			}else{
				let pointcloud = new Potree.PointCloudArena4D(geometry);
				loaded(pointcloud);
			}
		});
	}else{
		callback({"type": "loading_failed"});
	}
};

Potree.updatePointClouds = function(pointclouds, camera, renderer){

	if(!Potree.lru){
		Potree.lru = new LRU();
	}

	for(let i = 0; i < pointclouds.length; i++){
		let pointcloud = pointclouds[i];
		for(let j = 0; j < pointcloud.profileRequests.length; j++){
			pointcloud.profileRequests[j].update();
		}
	}

	let result = Potree.updateVisibility(pointclouds, camera, renderer);

	for(let i = 0; i < pointclouds.length; i++){
		let pointcloud = pointclouds[i];
		pointcloud.updateMaterial(pointcloud.material, pointcloud.visibleNodes, camera, renderer);
		pointcloud.updateVisibleBounds();
	}

	Potree.getLRU().freeMemory();

	return result;
};

Potree.getLRU = function(){
	if(!Potree.lru){
		Potree.lru = new LRU();
	}

	return Potree.lru;
};






Potree.PointCloudTree = class PointCloudTree extends THREE.Object3D{

	constructor(){
		super();

		this.dem = new Potree.DEM(this);
	}

	initialized(){
		return this.root !== null;
	}


};


Potree.PointCloudOctree = class extends Potree.PointCloudTree{

	constructor(geometry, material){
		super();

		this.pcoGeometry = geometry;
		this.boundingBox = this.pcoGeometry.boundingBox;
		this.boundingSphere = this.boundingBox.getBoundingSphere();
		this.material = material || new Potree.PointCloudMaterial();
		this.visiblePointsTarget = 2*1000*1000;
		this.minimumNodePixelSize = 150;
		this.level = 0;
		this.position.copy(geometry.offset);
		this.updateMatrix();

		this.showBoundingBox = false;
		this.boundingBoxNodes = [];
		this.loadQueue = [];
		this.visibleBounds = new THREE.Box3();
		this.visibleNodes = [];
		this.visibleGeometry = [];
		this.generateDEM = false;
		this.profileRequests = [];
		this.name = "";

		{
			let box = [this.pcoGeometry.tightBoundingBox, this.getBoundingBoxWorld()]
				.find(v => v !== undefined);

			this.updateMatrixWorld(true);
			box = Potree.utils.computeTransformedBoundingBox(box, this.matrixWorld);

			let bWidth = box.max.z - box.min.z;
			let bMin = box.min.z - 0.2 * bWidth;
			let bMax = box.max.z + 0.2 * bWidth;
			this.material.heightMin = bMin;
			this.material.heightMax = bMax;
		}

		// TODO read projection from file instead
		this.projection = geometry.projection;

		this.root = this.pcoGeometry.root;
	}


	setName(name){
		if(this.name !== name){
			this.name = name;
			this.dispatchEvent({type: "name_changed", name: name, pointcloud: this});
		}
	}

	getName(){
		return name;
	}

	toTreeNode(geometryNode, parent){
		let node = new Potree.PointCloudOctreeNode();

		//if(geometryNode.name === "r40206"){
		//	console.log("creating node for r40206");
		//}
		let sceneNode = new THREE.Points(geometryNode.geometry, this.material);
		sceneNode.name = geometryNode.name;
		sceneNode.position.copy(geometryNode.boundingBox.min);
		sceneNode.frustumCulled = false;
		sceneNode.onBeforeRender = (_this, scene, camera, geometry, material, group) => {

			if(material.program){
				_this.getContext().useProgram(material.program.program);

				if(material.program.getUniforms().map.level){
					let level = geometryNode.getLevel();
					material.uniforms.level.value = level;
					material.program.getUniforms().map.level.setValue(_this.getContext(), level);
				}

				if(this.visibleNodeTextureOffsets && material.program.getUniforms().map.vnStart){
					let vnStart = this.visibleNodeTextureOffsets.get(node);
					material.uniforms.vnStart.value = vnStart;
					material.program.getUniforms().map.vnStart.setValue(_this.getContext(), vnStart);
				}

				if(material.program.getUniforms().map.pcIndex){
					let i = node.pcIndex ? node.pcIndex : this.visibleNodes.indexOf(node);
					material.uniforms.pcIndex.value = i;
					material.program.getUniforms().map.pcIndex.setValue(_this.getContext(), i);
				}
			}
		};

		//{ // DEBUG
		//	let sg = new THREE.SphereGeometry(1, 16, 16);
		//	let sm = new THREE.MeshNormalMaterial();
		//	let s = new THREE.Mesh(sg, sm);
		//	s.scale.set(5, 5, 5);
		//	s.position.copy(geometryNode.mean)
		//		.add(this.position)
		//		.add(geometryNode.boundingBox.min);
		//
		//	viewer.scene.scene.add(s);
		//}

		node.geometryNode = geometryNode;
		node.sceneNode = sceneNode;
		node.pointcloud = this;
		node.children = {};
		for(let key in geometryNode.children){
			node.children[key] = geometryNode.children[key];
		}

		if(!parent){
			this.root = node;
			this.add(sceneNode);
		}else{
			let childIndex = parseInt(geometryNode.name[geometryNode.name.length - 1]);
			parent.sceneNode.add(sceneNode);
			parent.children[childIndex] = node;
		}

		let disposeListener = function(){
			let childIndex = parseInt(geometryNode.name[geometryNode.name.length - 1]);
			parent.sceneNode.remove(node.sceneNode);
			parent.children[childIndex] = geometryNode;
		}
		geometryNode.oneTimeDisposeHandlers.push(disposeListener);

		return node;
	}

	updateVisibleBounds(){
		let leafNodes = [];
		for(let i = 0; i < this.visibleNodes.length; i++){
			let node = this.visibleNodes[i];
			let isLeaf = true;

			for(let j = 0; j < node.children.length; j++){
				let child = node.children[j];
				if(child instanceof Potree.PointCloudOctreeNode){
					isLeaf = isLeaf && !child.sceneNode.visible;
				}else if(child instanceof Potree.PointCloudOctreeGeometryNode){
					isLeaf = true;
				}
			}

			if(isLeaf){
				leafNodes.push(node);
			}
		}

		this.visibleBounds.min = new THREE.Vector3( Infinity, Infinity, Infinity );
		this.visibleBounds.max = new THREE.Vector3( - Infinity, - Infinity, - Infinity );
		for(let i = 0; i < leafNodes.length; i++){
			let node = leafNodes[i];

			this.visibleBounds.expandByPoint(node.getBoundingBox().min);
			this.visibleBounds.expandByPoint(node.getBoundingBox().max);
		}
	}

	updateMaterial(material, visibleNodes, camera, renderer){
		material.fov = camera.fov * (Math.PI / 180);
		material.screenWidth = renderer.domElement.clientWidth;
		material.screenHeight = renderer.domElement.clientHeight;
		material.spacing = this.pcoGeometry.spacing * Math.max(this.scale.x, this.scale.y, this.scale.z);
		material.near = camera.near;
		material.far = camera.far;
		material.uniforms.octreeSize.value = this.pcoGeometry.boundingBox.getSize().x;

		// update visibility texture
		if(material.pointSizeType >= 0){
			if(material.pointSizeType === Potree.PointSizeType.ADAPTIVE
				|| material.pointColorType === Potree.PointColorType.LOD){

				this.updateVisibilityTexture(material, visibleNodes);
			}
		}
	}

	updateVisibilityTexture(material, visibleNodes){
		if(!material){
			return;
		}

		let texture = material.visibleNodesTexture;
		let data = texture.image.data;
		data.fill(0);

		this.visibleNodeTextureOffsets = new Map();

		// copy array
		visibleNodes = visibleNodes.slice();

		// sort by level and index, e.g. r, r0, r3, r4, r01, r07, r30, ...
		let sort = function(a, b){
			let na = a.geometryNode.name;
			let nb = b.geometryNode.name;
			if(na.length != nb.length) return na.length - nb.length;
			if(na < nb) return -1;
			if(na > nb) return 1;
			return 0;
		};
		visibleNodes.sort(sort);


		for(let i = 0; i < visibleNodes.length; i++){
			let node = visibleNodes[i];

			this.visibleNodeTextureOffsets.set(node, i);

			let children = [];
			for(let j = 0; j < 8; j++){
				let child = node.children[j];
				if(child instanceof Potree.PointCloudOctreeNode && child.sceneNode.visible && visibleNodes.indexOf(child) >= 0){
					children.push(child);
				}
			}
			children.sort(function(a, b){
				if(a.geometryNode.name < b.geometryNode.name) return -1;
				if(a.geometryNode.name > b.geometryNode.name) return 1;
				return 0;
			});

			data[i*3 + 0] = 0;
			data[i*3 + 1] = 0;
			data[i*3 + 2] = 0;
			for(let j = 0; j < children.length; j++){
				let child = children[j];
				let index = parseInt(child.geometryNode.name.substr(-1));
				data[i*3 + 0] += Math.pow(2, index);

				if(j === 0){
					let vArrayIndex = visibleNodes.indexOf(child);
					data[i*3 + 1] = (vArrayIndex - i) >> 8;
					data[i*3 + 2] = (vArrayIndex - i) % 256;
				}

			}
		}


		texture.needsUpdate = true;
	}

	nodeIntersectsProfile(node, profile){
		let bbWorld = node.boundingBox.clone().applyMatrix4(this.matrixWorld);
		let bsWorld = bbWorld.getBoundingSphere();

		for(let i = 0; i < profile.points.length - 1; i++){
			let start = new THREE.Vector3(profile.points[i].x, bsWorld.center.y, profile.points[i].z);
			let end = new THREE.Vector3(profile.points[i+1].x, bsWorld.center.y, profile.points[i+1].z);

			let ray1 = new THREE.Ray(start, new THREE.Vector3().subVectors(end, start).normalize());
			let ray2 = new THREE.Ray(end, new THREE.Vector3().subVectors(start, end).normalize());

			if(ray1.intersectsSphere(bsWorld) && ray2.intersectsSphere(bsWorld)){
				return true;
			}
		}

		return false;
	}

	nodesOnRay(nodes, ray){
		let nodesOnRay = [];

		let _ray = ray.clone();
		for(let i = 0; i < nodes.length; i++){
			let node = nodes[i];
			//var inverseWorld = new THREE.Matrix4().getInverse(node.matrixWorld);
			//let sphere = node.getBoundingSphere().clone().applyMatrix4(node.sceneNode.matrixWorld);
			let sphere = node.getBoundingSphere().clone().applyMatrix4(this.matrixWorld);

			if(_ray.intersectsSphere(sphere)){
				nodesOnRay.push(node);
			}
		}

		return nodesOnRay;
	}

	updateMatrixWorld( force ){
		if ( this.matrixAutoUpdate === true ) this.updateMatrix();

		if ( this.matrixWorldNeedsUpdate === true || force === true ) {

			if ( !this.parent ) {

				this.matrixWorld.copy( this.matrix );

			} else {

				this.matrixWorld.multiplyMatrices( this.parent.matrixWorld, this.matrix );

			}

			this.matrixWorldNeedsUpdate = false;

			force = true;

		}
	}

	hideDescendants(object){
		let stack = [];
		for(let i = 0; i < object.children.length; i++){
			let child = object.children[i];
			if(child.visible){
				stack.push(child);
			}
		}

		while(stack.length > 0){
			let object = stack.shift();

			object.visible = false;

			for(let i = 0; i < object.children.length; i++){
				let child = object.children[i];
				if(child.visible){
					stack.push(child);
				}
			}
		}
	}

	moveToOrigin(){
		this.position.set(0,0,0);
		this.updateMatrixWorld(true);
		let box = this.boundingBox;
		let transform = this.matrixWorld;
		let tBox = Potree.utils.computeTransformedBoundingBox(box, transform);
		this.position.set(0,0,0).sub(tBox.getCenter());
	};

	moveToGroundPlane(){
		this.updateMatrixWorld(true);
		let box = this.boundingBox;
		let transform = this.matrixWorld;
		let tBox = Potree.utils.computeTransformedBoundingBox(box, transform);
		this.position.y += -tBox.min.y;
	};

	getBoundingBoxWorld(){
		this.updateMatrixWorld(true);
		let box = this.boundingBox;
		let transform = this.matrixWorld;
		let tBox = Potree.utils.computeTransformedBoundingBox(box, transform);

		return tBox;
	};

	/**
	 * returns points inside the profile points
	 *
	 * maxDepth:		search points up to the given octree depth
	 *
	 *
	 * The return value is an array with all segments of the profile path
	 *  var segment = {
	 * 		start: 	THREE.Vector3,
	 * 		end: 	THREE.Vector3,
	 * 		points: {}
	 * 		project: function()
	 *  };
	 *
	 * The project() function inside each segment can be used to transform
	 * that segments point coordinates to line up along the x-axis.
	 *
	 *
	 */
	getPointsInProfile(profile, maxDepth, callback){

		if(callback){
			let request = new Potree.ProfileRequest(this, profile, maxDepth, callback);
			this.profileRequests.push(request);

			return request;
		}

		let points = {
			segments: [],
			boundingBox: new THREE.Box3(),
			projectedBoundingBox: new THREE.Box2()
		};

		// evaluate segments
		for(let i = 0; i < profile.points.length - 1; i++){
			let start = profile.points[i];
			let end = profile.points[i+1];
			let ps = this.getProfile(start, end, profile.width, maxDepth);

			let segment = {
				start: start,
				end: end,
				points: ps,
				project: null
			};

			points.segments.push(segment);

			points.boundingBox.expandByPoint(ps.boundingBox.min);
			points.boundingBox.expandByPoint(ps.boundingBox.max);
		}

		// add projection functions to the segments
		let mileage = new THREE.Vector3();
		for(let i = 0; i < points.segments.length; i++){
			let segment = points.segments[i];
			let start = segment.start;
			let end = segment.end;

			let project = function(_start, _end, _mileage, _boundingBox){
				let start = _start;
				let end = _end;
				let mileage = _mileage;
				let boundingBox = _boundingBox;

				let xAxis = new THREE.Vector3(1,0,0);
				let dir = new THREE.Vector3().subVectors(end, start);
				dir.y = 0;
				dir.normalize();
				let alpha = Math.acos(xAxis.dot(dir));
				if(dir.z > 0){
					alpha = -alpha;
				}


				return function(position){

					let toOrigin = new THREE.Matrix4().makeTranslation(-start.x, -boundingBox.min.y, -start.z);
					let alignWithX = new THREE.Matrix4().makeRotationY(-alpha);
					let applyMileage = new THREE.Matrix4().makeTranslation(mileage.x, 0, 0);

					let pos = position.clone();
					pos.applyMatrix4(toOrigin);
					pos.applyMatrix4(alignWithX);
					pos.applyMatrix4(applyMileage);

					return pos;
				};

			}(start, end, mileage.clone(), points.boundingBox.clone());

			segment.project = project;

			mileage.x += new THREE.Vector3(start.x, 0, start.z).distanceTo(new THREE.Vector3(end.x, 0, end.z));
			mileage.y += end.y - start.y;
		}

		points.projectedBoundingBox.min.x = 0;
		points.projectedBoundingBox.min.y = points.boundingBox.min.y;
		points.projectedBoundingBox.max.x = mileage.x;
		points.projectedBoundingBox.max.y = points.boundingBox.max.y;

		return points;
	}

	/**
	 * returns points inside the given profile bounds.
	 *
	 * start:
	 * end:
	 * width:
	 * depth:		search points up to the given octree depth
	 * callback:	if specified, points are loaded before searching
	 *
	 *
	 */
	getProfile(start, end, width, depth, callback){
		if(callback !== undefined){
			let request = new Potree.ProfileRequest(start, end, width, depth, callback);
			this.profileRequests.push(request);
		}else{
			let stack = [];
			stack.push(this);

			let center = new THREE.Vector3().addVectors(end, start).multiplyScalar(0.5);
			let length = new THREE.Vector3().subVectors(end, start).length();
			let side = new THREE.Vector3().subVectors(end, start).normalize();
			let up = new THREE.Vector3(0, 1, 0);
			let forward = new THREE.Vector3().crossVectors(side, up).normalize();
			let N = forward;
			let cutPlane = new THREE.Plane().setFromNormalAndCoplanarPoint(N, start);
			let halfPlane = new THREE.Plane().setFromNormalAndCoplanarPoint(side, center);

			let inside = null;

			let boundingBox = new THREE.Box3();


			while(stack.length > 0){
				let object = stack.shift();


				let pointsFound = 0;

				if(object instanceof THREE.Points){
					let geometry = object.geometry;
					let positions = geometry.attributes.position;
					let p = positions.array;
					let numPoints = object.numPoints;

					if(!inside){
						inside = {};

						for (let property in geometry.attributes) {
							if (geometry.attributes.hasOwnProperty(property)) {
								if(property === "indices"){

								}else{
									inside[property] = [];
								}
							}
						}
					}

					for(let i = 0; i < numPoints; i++){
						let pos = new THREE.Vector3(p[3*i], p[3*i+1], p[3*i+2]);
						pos.applyMatrix4(this.matrixWorld);
						let distance = Math.abs(cutPlane.distanceToPoint(pos));
						let centerDistance = Math.abs(halfPlane.distanceToPoint(pos));

						if(distance < width / 2 && centerDistance < length / 2){
							boundingBox.expandByPoint(pos);

							for (let property in geometry.attributes) {
								if (geometry.attributes.hasOwnProperty(property)) {

									if(property === "position"){
										inside[property].push(pos);
									}else if(property === "indices"){
										// skip indices
									}else{
										let values = geometry.attributes[property];
										if(values.itemSize === 1){
											inside[property].push(values.array[i + j]);
										}else{
											let value = [];
											for(let j = 0; j < values.itemSize; j++){
												value.push(values.array[i*values.itemSize + j]);
											}
											inside[property].push(value);
										}
									}

								}
							}


							pointsFound++;
						}
					}
				}

				//console.log("traversing: " + object.name + ", #points found: " + pointsFound);

				if(object == this || object.level < depth){
					for(let i = 0; i < object.children.length; i++){
						let child = object.children[i];
						if(child instanceof THREE.Points){
							let sphere = child.boundingSphere.clone().applyMatrix4(child.matrixWorld);
							if(cutPlane.distanceToSphere(sphere) < sphere.radius){
								stack.push(child);
							}
						}
					}
				}
			}

			inside.numPoints = inside.position.length;

			let project = function(_start, _end){
				let start = _start;
				let end = _end;

				let xAxis = new THREE.Vector3(1,0,0);
				let dir = new THREE.Vector3().subVectors(end, start);
				dir.y = 0;
				dir.normalize();
				let alpha = Math.acos(xAxis.dot(dir));
				if(dir.z > 0){
					alpha = -alpha;
				}


				return function(position){

					let toOrigin = new THREE.Matrix4().makeTranslation(-start.x, -start.y, -start.z);
					let alignWithX = new THREE.Matrix4().makeRotationY(-alpha);

					let pos = position.clone();
					pos.applyMatrix4(toOrigin);
					pos.applyMatrix4(alignWithX);

					return pos;
				};

			}(start, end);

			inside.project = project;
			inside.boundingBox = boundingBox;

			return inside;
		}
	};

	getVisibleExtent(){
		return this.visibleBounds.applyMatrix4(this.matrixWorld);
	};


	/**
	 *
	 *
	 *
	 * params.pickWindowSize:	Look for points inside a pixel window of this size.
	 * 							Use odd values: 1, 3, 5, ...
	 *
	 *
	 * TODO: only draw pixels that are actually read with readPixels().
	 *
	 */
	pick(renderer, camera, ray, params = {}){

		//let start = new Date().getTime();

		let pickWindowSize = params.pickWindowSize || 17;
		let pickOutsideClipRegion = params.pickOutsideClipRegion || false;
		let width = Math.ceil(renderer.domElement.clientWidth);
		let height = Math.ceil(renderer.domElement.clientHeight);

		let nodes = this.nodesOnRay(this.visibleNodes, ray);

		if(nodes.length === 0){
			return null;
		}

		if(!this.pickState){

			let scene = new THREE.Scene();

			let material = new Potree.PointCloudMaterial();
			material.pointColorType = Potree.PointColorType.POINT_INDEX;

			let renderTarget = new THREE.WebGLRenderTarget(
				1, 1,
				{ minFilter: THREE.LinearFilter,
				magFilter: THREE.NearestFilter,
				format: THREE.RGBAFormat }
			);

			this.pickState = {
				renderTarget: renderTarget,
				material: material,
				scene: scene
			};
		};

		let pickState = this.pickState;
		let pickMaterial = pickState.material;

		{ // update pick material
			pickMaterial.pointSizeType = this.material.pointSizeType;
			pickMaterial.shape = this.material.shape;

			pickMaterial.size = this.material.size;
			pickMaterial.minSize = this.material.minSize;
			pickMaterial.maxSize = this.material.maxSize;
			pickMaterial.classification = this.material.classification;

			if(pickOutsideClipRegion){
				pickMaterial.clipMode = Potree.ClipMode.DISABLED;
			}else{
				pickMaterial.clipMode = this.material.clipMode;
				if(this.material.clipMode === Potree.ClipMode.CLIP_OUTSIDE){
					pickMaterial.setClipBoxes(this.material.clipBoxes);
				}else{
					pickMaterial.setClipBoxes([]);
				}
			}

			this.updateMaterial(pickMaterial, nodes, camera, renderer);
		}

		if(pickState.renderTarget.width != width || pickState.renderTarget.height != height){
			pickState.renderTarget.dispose();
			pickState.renderTarget = new THREE.WebGLRenderTarget(
				1, 1,
				{ minFilter: THREE.LinearFilter,
				magFilter: THREE.NearestFilter,
				format: THREE.RGBAFormat }
			);
		}
		pickState.renderTarget.setSize(width, height);
		renderer.setRenderTarget( pickState.renderTarget );

		let pixelPos = new THREE.Vector3()
			.addVectors(camera.position, ray.direction)
			.project(camera)
			.addScalar(1)
			.multiplyScalar(0.5);
		pixelPos.x *= width;
		pixelPos.y *= height;

		renderer.setScissor(
			parseInt(pixelPos.x - (pickWindowSize - 1) / 2),
			parseInt(pixelPos.y - (pickWindowSize - 1) / 2),
			parseInt(pickWindowSize), parseInt(pickWindowSize));
		renderer.setScissorTest(true);

		renderer.state.buffers.depth.setTest(pickMaterial.depthTest);
		renderer.state.buffers.depth.setMask(pickMaterial.depthWrite);
		renderer.state.setBlending(THREE.NoBlending);

		renderer.clearTarget( pickState.renderTarget, true, true, true );

		//pickState.scene.children = nodes.map(n => n.sceneNode);

		//let childStates = [];
		let tempNodes = [];
		for(let i = 0; i < nodes.length; i++){
			let node = nodes[i];
			node.pcIndex = i+1;
			let sceneNode = node.sceneNode;

			let tempNode = new THREE.Points(sceneNode.geometry, pickMaterial);
			tempNode.matrix = sceneNode.matrix;
			tempNode.matrixWorld = sceneNode.matrixWorld;
			tempNode.matrixAutoUpdate = false;
			tempNode.frustumCulled = false;
			tempNode.pcIndex = i+1;

			let geometryNode = node.geometryNode;
			let material = pickMaterial;
			tempNode.onBeforeRender = (_this, scene, camera, geometry, material, group) => {

				if(material.program){
					_this.getContext().useProgram(material.program.program);

					if(material.program.getUniforms().map.level){
						let level = geometryNode.getLevel();
						material.uniforms.level.value = level;
						material.program.getUniforms().map.level.setValue(_this.getContext(), level);
					}

					if(this.visibleNodeTextureOffsets && material.program.getUniforms().map.vnStart){
						let vnStart = this.visibleNodeTextureOffsets.get(node);
						material.uniforms.vnStart.value = vnStart;
						material.program.getUniforms().map.vnStart.setValue(_this.getContext(), vnStart);
					}

					if(material.program.getUniforms().map.pcIndex){
						material.uniforms.pcIndex.value = node.pcIndex;
						material.program.getUniforms().map.pcIndex.setValue(_this.getContext(), node.pcIndex);
					}
				}

			};
			tempNodes.push(tempNode);

			//for(let child of nodes[i].sceneNode.children){
			//	childStates.push({child: child, visible: child.visible});
			//	child.visible = false;
			//}
		}
		pickState.scene.autoUpdate = false;
		pickState.scene.children = tempNodes;
		//pickState.scene.overrideMaterial = pickMaterial;

		renderer.state.setBlending(THREE.NoBlending);

		// RENDER
		renderer.render(pickState.scene, camera, pickState.renderTarget);

		//for(let childState of childStates){
		//	childState.child = childState.visible;
		//}

		//pickState.scene.overrideMaterial = null;

		//renderer.context.readPixels(
		//	pixelPos.x - (pickWindowSize-1) / 2, pixelPos.y - (pickWindowSize-1) / 2,
		//	pickWindowSize, pickWindowSize,
		//	renderer.context.RGBA, renderer.context.UNSIGNED_BYTE, pixels);

		let clamp = (number, min, max) => Math.min(Math.max(min, number), max);

		let x = parseInt(clamp(pixelPos.x - (pickWindowSize-1) / 2, 0, width));
		let y = parseInt(clamp(pixelPos.y - (pickWindowSize-1) / 2, 0, height));
		let w = parseInt(Math.min(x + pickWindowSize, width) - x);
		let h = parseInt(Math.min(y + pickWindowSize, height) - y);

		let pixelCount = w * h;
		let buffer = new Uint8Array(4 * pixelCount);
		renderer.readRenderTargetPixels(pickState.renderTarget,
			x, y, w, h,
			buffer);

		renderer.setScissorTest(false);

		renderer.setRenderTarget(null);

		let pixels = buffer;
		let ibuffer = new Uint32Array(buffer.buffer);

		// find closest hit inside pixelWindow boundaries
		let min = Number.MAX_VALUE;
		let hit = null;
		for(let u = 0; u < pickWindowSize; u++){
			for(let v = 0; v < pickWindowSize; v++){
				let offset = (u + v*pickWindowSize);
				let distance = Math.pow(u - (pickWindowSize-1) / 2, 2) + Math.pow(v - (pickWindowSize-1) / 2, 2);

				let pcIndex = pixels[4*offset + 3];
				pixels[4*offset + 3] = 0;
				let pIndex = ibuffer[offset];

				//if((pIndex !== 0 || pcIndex !== 0) && distance < min){
				if(pcIndex > 0 && distance < min){


					hit = {
						pIndex: pIndex,
						pcIndex: pcIndex - 1
					};
					min = distance;

					//console.log(hit);
				}
			}
		}
		//console.log(pixels);

		//{ // open window with image
		//	let img = Potree.utils.pixelsArrayToImage(buffer, w, h);
		//	let screenshot = img.src;
		//
		//	if(!this.debugDIV){
		//		this.debugDIV = $(`
		//			<div id="pickDebug"
		//			style="position: absolute;
		//			right: 400px; width: 300px;
		//			bottom: 44px; width: 300px;
		//			z-index: 1000;
		//			"></div>`);
		//		$(document.body).append(this.debugDIV);
		//	}
		//
		//	this.debugDIV.empty();
		//	this.debugDIV.append($(`<img src="${screenshot}"
		//		style="transform: scaleY(-1);"/>`));
		//	//$(this.debugWindow.document).append($(`<img src="${screenshot}"/>`));
		//	//this.debugWindow.document.write('<img src="'+screenshot+'"/>');
		//}

		//return;

		let point = null;

		if(hit){
			point = {};

			if(!nodes[hit.pcIndex]){
				return null;
			}

			let pc = nodes[hit.pcIndex].sceneNode;
			let attributes = pc.geometry.attributes;

			for(let property in attributes) {
				if (attributes.hasOwnProperty(property)) {
					let values = pc.geometry.attributes[property];

					if(property === "position"){
						let positionArray = values.array;
						let x = positionArray[3*hit.pIndex+0];
						let y = positionArray[3*hit.pIndex+1];
						let z = positionArray[3*hit.pIndex+2];
						let position = new THREE.Vector3(x, y, z);
						position.applyMatrix4(pc.matrixWorld);

						point[property] = position;
					}else if(property === "indices"){

					}else{
						if(values.itemSize === 1){
							point[property] = values.array[hit.pIndex];
						}else{
							let value = [];
							for(let j = 0; j < values.itemSize; j++){
								value.push(values.array[values.itemSize*hit.pIndex + j]);
							}
							point[property] = value;
						}
					}
				}
			}
		}

		//let end = new Date().getTime();
		//let duration = end - start;
		//console.log(`pick duration: ${duration}ms`);

		return point;


	};

	get progress(){
		return this.visibleNodes.length / this.visibleGeometry.length;
	}
};


var nodesLoadTimes = {};

Potree.PointCloudOctreeGeometry = function(){
	this.url = null;
	this.octreeDir = null;
	this.spacing = 0;
	this.boundingBox = null;
	this.root = null;
	this.numNodesLoading = 0;
	this.nodes = null;
	this.pointAttributes = null;
	this.hierarchyStepSize = -1;
	this.loader = null;
};

Potree.PointCloudOctreeGeometryNode = function(name, pcoGeometry, boundingBox){
	this.id = Potree.PointCloudOctreeGeometryNode.IDCount++;
	this.name = name;
	this.index = parseInt(name.charAt(name.length-1));
	this.pcoGeometry = pcoGeometry;
	this.geometry = null;
	this.boundingBox = boundingBox;
	this.boundingSphere = boundingBox.getBoundingSphere();
	this.children = {};
	this.numPoints = 0;
	this.level = null;
	this.loaded = false;
	this.oneTimeDisposeHandlers = [];
};

Potree.DEM = class DEM{
	constructor(pointcloud){

		this.pointcloud = pointcloud;
		this.matrix = null;
		this.boundingBox = null;
		this.tileSize = 64;
		this.root = null;
		this.version = 0;

	}

	// expands the tree to all nodes that intersect <box> at <level>
	// returns the intersecting nodes at <level>
	expandAndFindByBox(box, level){

		if(level === 0){
			return [this.root];
		}

		let result = [];
		let stack = [this.root];

		while(stack.length > 0){

			let node = stack.pop();
			let nodeBoxSize = node.box.getSize();

			// check which children intersect by transforming min/max to quadrants
			let min = {
				x: (box.min.x - node.box.min.x) / nodeBoxSize.x,
				y: (box.min.y - node.box.min.y) / nodeBoxSize.y};
			let max = {
				x: (box.max.x - node.box.max.x) / nodeBoxSize.x,
				y: (box.max.y - node.box.max.y) / nodeBoxSize.y};

			min.x = min.x < 0.5 ? 0 : 1;
			min.y = min.y < 0.5 ? 0 : 1;
			max.x = max.x < 0.5 ? 0 : 1;
			max.y = max.y < 0.5 ? 0 : 1;

			let childIndices;
			if(min.x === 0 && min.y === 0 && max.x === 1 && max.y === 1){
				childIndices = [0, 1, 2, 3];
			}else if(min.x === max.x && min.y == max.y){
				childIndices = [(min.x << 1) | min.y];
			}else{
				childIndices = [(min.x << 1) | min.y, (max.x << 1) | max.y];
			}

			for(let index of childIndices){
				if(node.children[index] === undefined){
					let childBox = node.box.clone();

					if((index & 2) > 0){
						childBox.min.x += nodeBoxSize.x / 2.0;
					}else{
						childBox.max.x -= nodeBoxSize.x / 2.0;
					}

					if((index & 1) > 0){
						childBox.min.y += nodeBoxSize.y / 2.0;
					}else{
						childBox.max.y -= nodeBoxSize.y / 2.0;
					}

					let child = new Potree.DEMNode(node.name + index, childBox, this.tileSize);
					node.children[index] = child;
				}

				let child = node.children[index];

				if(child.level < level){
					stack.push(child);
				}else{
					result.push(child);
				}
			}
		}

		return result;
	}

	childIndex(uv){
		let [x, y] = uv.map(n => n < 0.5 ? 0 : 1);

		let index = (x << 1) | y;

		return index;
	}

	height(position){

		//return this.root.height(position);

		if(!this.root){
			return 0;
		}

		let height = null;
		let list = [this.root];
		while(true){
			let node = list[list.length - 1];

			let currentHeight = node.height(position);

			if(currentHeight !== null){
				height = currentHeight;
			}

			let uv = node.uv(position);
			let childIndex = this.childIndex(uv);

			if(node.children[childIndex]){
				list.push(node.children[childIndex]);
			}else{
				break;
			}
		}

		return height + this.pointcloud.position.z;
	}

	update(visibleNodes){

		if(Potree.getDEMWorkerInstance().working){
			return;
		}

		// check if point cloud transformation changed
		if(this.matrix === null || !this.matrix.equals(this.pointcloud.matrixWorld)){
			this.matrix = this.pointcloud.matrixWorld.clone();
			this.boundingBox = this.pointcloud.boundingBox.clone().applyMatrix4(this.matrix);
			this.root = new Potree.DEMNode("r", this.boundingBox, this.tileSize);
			this.version++;
		}

		// find node to update
		let node = null;
		for(let vn of visibleNodes){
			if(vn.demVersion === undefined || vn.demVersion < this.version){
				node = vn;
				break;
			}
		}
		if(node === null){
			return;
		}


		// update node
		let projectedBox = node.getBoundingBox().clone().applyMatrix4(this.matrix);
		let projectedBoxSize = projectedBox.getSize();

		let targetNodes = this.expandAndFindByBox(projectedBox, node.getLevel());
		node.demVersion = this.version;

		Potree.getDEMWorkerInstance().onmessage = (e) => {

			let data = new Float32Array(e.data.dem.data);

			for(let demNode of targetNodes){

				let boxSize = demNode.box.getSize();

				for(let i = 0; i < this.tileSize; i++){
					for(let j = 0; j < this.tileSize; j++){

						let u = (i / (this.tileSize - 1));
						let v = (j / (this.tileSize - 1));

						let x = demNode.box.min.x + u * boxSize.x;
						let y = demNode.box.min.y + v * boxSize.y;

						let ix = this.tileSize * (x - projectedBox.min.x) / projectedBoxSize.x;
						let iy = this.tileSize * (y - projectedBox.min.y) / projectedBoxSize.y;

						if(ix < 0 || ix > this.tileSize){
							continue;
						}

						if(iy < 0 || iy > this.tileSize){
							continue;
						}

						ix = Math.min(Math.floor(ix), this.tileSize - 1);
						iy = Math.min(Math.floor(iy), this.tileSize - 1);

						demNode.data[i + this.tileSize * j] = data[ix + this.tileSize * iy];

					}
				}

				demNode.createMipMap();
				demNode.mipMapNeedsUpdate = true;

				Potree.getDEMWorkerInstance().working = false;



			}



			// TODO only works somewhat if there is no rotation to the point cloud

			//let target = targetNodes[0];
			//target.data = new Float32Array(data);
			//
			//
			////node.dem = e.data.dem;
		    //
			//Potree.getDEMWorkerInstance().working = false;
			//
			//{ // create scene objects for debugging
			//	//for(let demNode of targetNodes){
			//		var bb = new Potree.Box3Helper(box);
			//		viewer.scene.scene.add(bb);
            //
			//		createDEMMesh(this, target);
			//	//}
			//
			//}
		};

		let position = node.geometryNode.geometry.attributes.position.array;
		let message = {
			boundingBox: {
				min: node.getBoundingBox().min.toArray(),
				max: node.getBoundingBox().max.toArray()
			},
			position: new Float32Array(position).buffer
		};
		let transferables = [message.position];
		Potree.getDEMWorkerInstance().working = true;
		Potree.getDEMWorkerInstance().postMessage(message, transferables);


	}

};



Potree.PointCloudMaterial = class PointCloudMaterial extends THREE.RawShaderMaterial{

	constructor(parameters = {}){
		super();

		this.visibleNodesTexture = Potree.utils.generateDataTexture( 2048, 1, new THREE.Color( 0xffffff ) );
		this.visibleNodesTexture.minFilter = THREE.NearestFilter;
		this.visibleNodesTexture.magFilter = THREE.NearestFilter;

		let pointSize = parameters.size || 1.0;
		let minSize = parameters.minSize || 1.0;
		let maxSize = parameters.maxSize || 50.0;
		let treeType = parameters.treeType || Potree.TreeType.OCTREE;

		this._pointSizeType = Potree.PointSizeType.FIXED;
		this._shape = Potree.PointShape.SQUARE;
		this._pointColorType = Potree.PointColorType.RGB;
		this._useClipBox = false;
		this.numClipBoxes = 0;
		this._clipMode = Potree.ClipMode.DISABLED;
		this._weighted = false;
		this._depthMap = null;
		this._gradient = Potree.Gradients.SPECTRAL;
		this._classification = Potree.Classification.DEFAULT;
		this.gradientTexture = Potree.PointCloudMaterial.generateGradientTexture(this._gradient);
		this.classificationTexture = Potree.PointCloudMaterial.generateClassificationTexture(this._classification);
		this.lights = false;
		this.fog = false;
		this._treeType = treeType;
		this._useEDL = false;

		this._defaultIntensityRangeChanged = false;
		this._defaultElevationRangeChanged = false;

		this.attributes = {
			position: 			{ type: "fv", value: [] },
			color: 				{ type: "fv", value: [] },
			normal: 			{ type: "fv", value: [] },
			intensity: 			{ type: "f", value: [] },
			classification: 	{ type: "f", value: [] },
			returnNumber: 		{ type: "f", value: [] },
			numberOfReturns: 	{ type: "f", value: [] },
			pointSourceID: 		{ type: "f", value: [] },
			indices: 			{ type: "fv", value: [] }
		};

		this.uniforms = {
			level:				{ type: "f", value: 0.0 },
			vnStart:				{ type: "f", value: 0.0 },
			spacing:			{ type: "f", value: 1.0 },
			blendHardness:		{ type: "f", value: 2.0 },
			blendDepthSupplement:	{ type: "f", value: 0.0 },
			fov:				{ type: "f", value: 1.0 },
			screenWidth:		{ type: "f", value: 1.0 },
			screenHeight:		{ type: "f", value: 1.0 },
			near:				{ type: "f", value: 0.1 },
			far:				{ type: "f", value: 1.0 },
			uColor:   			{ type: "c", value: new THREE.Color( 0xffffff ) },
			opacity:   			{ type: "f", value: 1.0 },
			size:   			{ type: "f", value: pointSize },
			minSize:   			{ type: "f", value: minSize },
			maxSize:   			{ type: "f", value: maxSize },
			octreeSize:			{ type: "f", value: 0 },
			bbSize:				{ type: "fv", value: [0,0,0] },
			heightMin:			{ type: "f", value: 0.0 },
			heightMax:			{ type: "f", value: 1.0 },
			clipBoxCount:		{ type: "f", value: 0 },
			visibleNodes:		{ type: "t", value: this.visibleNodesTexture },
			pcIndex:   			{ type: "f", value: 0 },
			gradient: 			{ type: "t", value: this.gradientTexture },
			classificationLUT: 	{ type: "t", value: this.classificationTexture },
			clipBoxes:			{ type: "Matrix4fv", value: [] },
			toModel:			{ type: "Matrix4f", value: [] },
			depthMap: 			{ type: "t", value: null },
			diffuse:			{ type: "fv", value: [1,1,1]},
			transition:         { type: "f", value: 0.5 },
			intensityRange:     { type: "fv", value: [0, 65000] },
			intensityGamma:     { type: "f", value: 1 },
			intensityContrast:	{ type: "f", value: 0 },
			intensityBrightness:{ type: "f", value: 0 },
			rgbGamma:     		{ type: "f", value: 1 },
			rgbContrast:		{ type: "f", value: 0 },
			rgbBrightness:		{ type: "f", value: 0 },
			wRGB:				{ type: "f", value: 1 },
			wIntensity:			{ type: "f", value: 0 },
			wElevation:			{ type: "f", value: 0 },
			wClassification:	{ type: "f", value: 0 },
			wReturnNumber:		{ type: "f", value: 0 },
			wSourceID:			{ type: "f", value: 0 },
		};

		this.defaultAttributeValues.normal = [0,0,0];
		this.defaultAttributeValues.classification = [0,0,0];
		this.defaultAttributeValues.indices = [0,0,0,0];

		this.vertexShader = this.getDefines() + Potree.Shaders["pointcloud.vs"];
		this.fragmentShader = this.getDefines() + Potree.Shaders["pointcloud.fs"];
		this.vertexColors = THREE.VertexColors;
	}

	updateShaderSource(){
		this.vertexShader = this.getDefines() + Potree.Shaders["pointcloud.vs"];
		this.fragmentShader = this.getDefines() + Potree.Shaders["pointcloud.fs"];

		if(this.depthMap){
			this.uniforms.depthMap.value = this.depthMap;
			//this.depthMap = depthMap;
			//this.setValues({
			//	depthMap: this.depthMap,
			//});
		}

		if(this.opacity === 1.0){
			this.blending = THREE.NoBlending;
			this.transparent = false;
			this.depthTest = true;
			this.depthWrite = true;
		}else if(this.opacity < 1.0 && !this.useEDL){
			this.blending = THREE.AdditiveBlending;
			this.transparent = true;
			this.depthTest = false;
			this.depthWrite = true;
			this.depthFunc = THREE.AlwaysDepth;
		}

		if(this.weighted){
			this.blending = THREE.AdditiveBlending;
			this.transparent = true;
			this.depthTest = true;
			this.depthWrite = false;
		}

		this.needsUpdate = true;
	}

	getDefines(){
		let defines = "";

		if(this.pointSizeType === Potree.PointSizeType.FIXED){
			defines += "#define fixed_point_size\n";
		}else if(this.pointSizeType === Potree.PointSizeType.ATTENUATED){
			defines += "#define attenuated_point_size\n";
		}else if(this.pointSizeType === Potree.PointSizeType.ADAPTIVE){
			defines += "#define adaptive_point_size\n";
		}

		if(this.shape === Potree.PointShape.SQUARE){
			defines += "#define square_point_shape\n";
		}else if(this.shape === Potree.PointShape.CIRCLE){
			defines += "#define circle_point_shape\n";
		}else if(this.shape === Potree.PointShape.PARABOLOID){
			defines += "#define paraboloid_point_shape\n";
		}

		if(this._useEDL){
			defines += "#define use_edl\n";
		}

		if(this._pointColorType === Potree.PointColorType.RGB){
			defines += "#define color_type_rgb\n";
		}else if(this._pointColorType === Potree.PointColorType.COLOR){
			defines += "#define color_type_color\n";
		}else if(this._pointColorType === Potree.PointColorType.DEPTH){
			defines += "#define color_type_depth\n";
		}else if(this._pointColorType === Potree.PointColorType.HEIGHT){
			defines += "#define color_type_height\n";
		}else if(this._pointColorType === Potree.PointColorType.INTENSITY){
			defines += "#define color_type_intensity\n";
		}else if(this._pointColorType === Potree.PointColorType.INTENSITY_GRADIENT){
			defines += "#define color_type_intensity_gradient\n";
		}else if(this._pointColorType === Potree.PointColorType.LOD){
			defines += "#define color_type_lod\n";
		}else if(this._pointColorType === Potree.PointColorType.POINT_INDEX){
			defines += "#define color_type_point_index\n";
		}else if(this._pointColorType === Potree.PointColorType.CLASSIFICATION){
			defines += "#define color_type_classification\n";
		}else if(this._pointColorType === Potree.PointColorType.RETURN_NUMBER){
			defines += "#define color_type_return_number\n";
		}else if(this._pointColorType === Potree.PointColorType.SOURCE){
			defines += "#define color_type_source\n";
		}else if(this._pointColorType === Potree.PointColorType.NORMAL){
			defines += "#define color_type_normal\n";
		}else if(this._pointColorType === Potree.PointColorType.PHONG){
			defines += "#define color_type_phong\n";
		}else if(this._pointColorType === Potree.PointColorType.RGB_HEIGHT){
			defines += "#define color_type_rgb_height\n";
		}else if(this._pointColorType === Potree.PointColorType.COMPOSITE){
			defines += "#define color_type_composite\n";
		}

		if(this.clipMode === Potree.ClipMode.DISABLED){
			defines += "#define clip_disabled\n";
		}else if(this.clipMode === Potree.ClipMode.CLIP_OUTSIDE){
			defines += "#define clip_outside\n";
		}else if(this.clipMode === Potree.ClipMode.HIGHLIGHT_INSIDE){
			defines += "#define clip_highlight_inside\n";
		}

		if(this._treeType === Potree.TreeType.OCTREE){
			defines += "#define tree_type_octree\n";
		}else if(this._treeType === Potree.TreeType.KDTREE){
			defines += "#define tree_type_kdtree\n";
		}

		if(this.weighted){
			defines += "#define weighted_splats\n";
		}

		if(this.numClipBoxes > 0){
			defines += "#define use_clip_box\n";
		}

		return defines;
	}


	setClipBoxes(clipBoxes){
		if(!clipBoxes){
			return;
		}

		this.clipBoxes = clipBoxes;
		let doUpdate = (this.numClipBoxes !== clipBoxes.length) && (clipBoxes.length === 0 || this.numClipBoxes === 0);

		this.numClipBoxes = clipBoxes.length;
		this.uniforms.clipBoxCount.value = this.numClipBoxes;

		if(doUpdate){
			this.updateShaderSource();
		}

		this.uniforms.clipBoxes.value = new Float32Array(this.numClipBoxes * 16);

		for(let i = 0; i < this.numClipBoxes; i++){
			let box = clipBoxes[i];

			this.uniforms.clipBoxes.value.set(box.inverse.elements, 16*i);
		}

		for(let i = 0; i < this.uniforms.clipBoxes.value.length; i++){
			if(Number.isNaN(this.uniforms.clipBoxes.value[i])){
				this.uniforms.clipBoxes.value[i] = Infinity;
			}
		}
	}

	get gradient(){
		return this._gradient;
	}

	set gradient(value){
		if(this._gradient !== value){
			this._gradient = value;
			this.gradientTexture = Potree.PointCloudMaterial.generateGradientTexture(this._gradient);
			this.uniforms.gradient.value = this.gradientTexture;
		}
	}



	get classification(){
		return this._classification;
	}

	set classification(value){

		let isEqual = Object.keys(value).length === Object.keys(this._classification).length;

		for(let key of Object.keys(value)){
			isEqual = isEqual && this._classification[key] !== undefined;
			isEqual = isEqual && value[key].equals(this._classification[key]);
		}


		if(!isEqual){
			recomputeClassification();
		}
	}

	recomputeClassification(){
		this.classificationTexture = Potree.PointCloudMaterial.generateClassificationTexture(this._classification);
		this.uniforms.classificationLUT.value = this.classificationTexture;

		this.dispatchEvent({
			type: "material_property_changed",
			target: this
		});
	}

	get spacing(){
		return this.uniforms.spacing.value;
	}

	set spacing(value){
		if(this.uniforms.spacing.value !== value){
			this.uniforms.spacing.value = value;
		}
	}



	get useClipBox(){
		return this._useClipBox;
	}

	set useClipBox(value){
		if(this._useClipBox !== value){
			this._useClipBox = value;
			this.updateShaderSource();
		}
	}


	get weighted(){
		return this._weighted;
	}

	set weighted(value){
		if(this._weighted !== value){
			this._weighted = value;
			this.updateShaderSource();
		}
	}


	get fov(){
		return this.uniforms.fov.value;
	}

	set fov(value){
		if(this.uniforms.fov.value !== value){
			this.uniforms.fov.value = value;
			//this.updateShaderSource();
		}
	}

	get screenWidth(){
		return this.uniforms.screenWidth.value;
	}

	set screenWidth(value){
		if(this.uniforms.screenWidth.value !== value){
			this.uniforms.screenWidth.value = value;
			//this.updateShaderSource();
		}
	}


	get screenHeight(){
		return this.uniforms.screenHeight.value;
	}

	set screenHeight(value){
		if(this.uniforms.screenHeight.value !== value){
			this.uniforms.screenHeight.value = value;
			//this.updateShaderSource();
		}
	}


	get near(){
		return this.uniforms.near.value;
	}

	set near(value){
		if(this.uniforms.near.value !== value){
			this.uniforms.near.value = value;
		}
	}


	get far(){
		return this.uniforms.far.value;
	}

	set far(value){
		if(this.uniforms.far.value !== value){
			this.uniforms.far.value = value;
		}
	}


	get opacity(){
		return this.uniforms.opacity.value;
	}

	set opacity(value){
		if(this.uniforms && this.uniforms.opacity){
			if(this.uniforms.opacity.value !== value){
				this.uniforms.opacity.value = value;
				this.updateShaderSource();
				this.dispatchEvent({
					type: "opacity_changed",
					target: this
				});
				this.dispatchEvent({
					type: "material_property_changed",
					target: this
				});
			}
		}
	}


	get pointColorType(){
		return this._pointColorType;
	}

	set pointColorType(value){
		if(this._pointColorType !== value){
			this._pointColorType = value;
			this.updateShaderSource();
			this.dispatchEvent({
				type: "point_color_type_changed",
				target: this
			});
			this.dispatchEvent({
				type: "material_property_changed",
				target: this
			});
		}
	}


	get depthMap(){
		return this._depthMap;
	}

	set depthMap(value){
		if(this._depthMap !== value){
			this._depthMap = value;
			this.updateShaderSource();
		}
	}


	get pointSizeType(){
		return this._pointSizeType;
	}

	set pointSizeType(value){
		if(this._pointSizeType !== value){
			this._pointSizeType = value;
			this.updateShaderSource();
			this.dispatchEvent({
				type: "point_size_type_changed",
				target: this
			});
			this.dispatchEvent({
				type: "material_property_changed",
				target: this
			});
		}
	}


	get clipMode(){
		return this._clipMode;
	}

	set clipMode(value){
		if(this._clipMode !== value){
			this._clipMode = value;
			this.updateShaderSource();
		}
	}


	get useEDL(){
		return this._useEDL;
	}

	set useEDL(value){
		if(this._useEDL !== value){
			this._useEDL = value;
			this.updateShaderSource();
		}
	}


	get color(){
		return this.uniforms.uColor.value;
	}

	set color(value){
		if(!this.uniforms.uColor.value.equals(value)){
			this.uniforms.uColor.value.copy(value);

			this.dispatchEvent({
				type: "color_changed",
				target: this
			});
			this.dispatchEvent({
				type: "material_property_changed",
				target: this
			});
		}
	}


	get shape(){
		return this._shape;
	}

	set shape(value){
		if(this._shape !== value){
			this._shape = value;
			this.updateShaderSource();
			this.dispatchEvent({type: "point_shape_changed", target: this});
			this.dispatchEvent({
				type: "material_property_changed",
				target: this
			});
		}
	}



	get treeType(){
		return this._treeType;
	}

	set treeType(value){
		if(this._treeType !== value){
			this._treeType = value;
			this.updateShaderSource();
		}
	}


	get bbSize(){
		return this.uniforms.bbSize.value;
	}

	set bbSize(value){
		this.uniforms.bbSize.value = value;
	}


	get size(){
		return this.uniforms.size.value;
	}

	set size(value){
		if(this.uniforms.size.value !== value){
			this.uniforms.size.value = value;

			this.dispatchEvent({
				type: "point_size_changed",
				target: this
			});
			this.dispatchEvent({
				type: "material_property_changed",
				target: this
			});
		}
	}

	get elevationRange(){
		return [this.heightMin, this.heightMax];
	}

	set elevationRange(value){
		this.heightMin = value[0];
		this.heightMax = value[1];
	}

	get heightMin(){
		return this.uniforms.heightMin.value;
	}

	set heightMin(value){
		if(this.uniforms.heightMin.value !== value){
			this.uniforms.heightMin.value = value;

			this._defaultElevationRangeChanged = true;

			this.dispatchEvent({
				type: "material_property_changed",
				target: this
			});
		}
	}


	get heightMax(){
		return this.uniforms.heightMax.value;
	}

	set heightMax(value){
		if(this.uniforms.heightMax.value !== value){
			this.uniforms.heightMax.value = value;

			this._defaultElevationRangeChanged = true;

			this.dispatchEvent({
				type: "material_property_changed",
				target: this
			});
		}
	}


	get transition(){
		return this.uniforms.transition.value;
	}

	set transition(value){
		this.uniforms.transition.value = value;
	}


	get intensityRange(){
		return this.uniforms.intensityRange.value;
	}

	set intensityRange(value){

		if(!(value instanceof Array && value.length === 2)){
			return;
		}

		if(value[0] === this.uniforms.intensityRange.value[0] &&
			value[1] === this.uniforms.intensityRange.value[1]){

			return;
		}

		this.uniforms.intensityRange.value = value;

		this._defaultIntensityRangeChanged = true;

		this.dispatchEvent({
			type: "material_property_changed",
			target: this
		});
	}


	get intensityGamma(){
		return this.uniforms.intensityGamma.value;
	}

	set intensityGamma(value){
		if(this.uniforms.intensityGamma.value !== value){
			this.uniforms.intensityGamma.value = value;
			this.dispatchEvent({
				type: "material_property_changed",
				target: this
			});
		}
	}


	get intensityContrast(){
		return this.uniforms.intensityContrast.value;
	}

	set intensityContrast(value){
		if(this.uniforms.intensityContrast.value !== value){
			this.uniforms.intensityContrast.value = value;
			this.dispatchEvent({
				type: "material_property_changed",
				target: this
			});
		}
	}


	get intensityBrightness(){
		return this.uniforms.intensityBrightness.value;
	}

	set intensityBrightness(value){
		if(this.uniforms.intensityBrightness.value !== value){
			this.uniforms.intensityBrightness.value = value;
			this.dispatchEvent({
				type: "material_property_changed",
				target: this
			});
		}
	}


	get rgbGamma(){
		return this.uniforms.rgbGamma.value;
	}

	set rgbGamma(value){
		if(this.uniforms.rgbGamma.value !== value){
			this.uniforms.rgbGamma.value = value;
			this.dispatchEvent({
				type: "material_property_changed",
				target: this
			});
		}
	}


	get rgbContrast(){
		return this.uniforms.rgbContrast.value;
	}

	set rgbContrast(value){
		if(this.uniforms.rgbContrast.value !== value){
			this.uniforms.rgbContrast.value = value;
			this.dispatchEvent({
				type: "material_property_changed",
				target: this
			});
		}
	}


	get rgbBrightness(){
		return this.uniforms.rgbBrightness.value;
	}

	set rgbBrightness(value){
		if(this.uniforms.rgbBrightness.value !== value){
			this.uniforms.rgbBrightness.value = value;
			this.dispatchEvent({
				type: "material_property_changed",
				target: this
			});
		}
	}


	get weightRGB(){
		return this.uniforms.wRGB.value;
	}

	set weightRGB(value){
		this.uniforms.wRGB.value = value;
	}


	get weightIntensity(){
		return this.uniforms.wIntensity.value;
	}

	set weightIntensity(value){
		this.uniforms.wIntensity.value = value;
	}


	get weightElevation(){
		return this.uniforms.wElevation.value;
	}

	set weightElevation(value){
		this.uniforms.wElevation.value = value;
	}


	get weightClassification(){
		return this.uniforms.wClassification.value;
	}

	set weightClassification(value){
		this.uniforms.wClassification.value = value;
	}


	get weightReturnNumber(){
		return this.uniforms.wReturnNumber.value;
	}

	set weightReturnNumber(value){
		this.uniforms.wReturnNumber.value = value;
	}


	get weightSourceID(){
		return this.uniforms.wSourceID.value;
	}

	set weightSourceID(value){
		this.uniforms.wSourceID.value = value;
	}



	static generateGradientTexture(gradient){
		let size = 64;

		// create canvas
		let canvas = document.createElement( 'canvas' );
		canvas.width = size;
		canvas.height = size;

		// get context
		let context = canvas.getContext( '2d' );

		// draw gradient
		context.rect( 0, 0, size, size );
		let ctxGradient = context.createLinearGradient( 0, 0, size, size );

		for(let i = 0;i < gradient.length; i++){
			let step = gradient[i];

			ctxGradient.addColorStop(step[0], "#" + step[1].getHexString());
		}

		context.fillStyle = ctxGradient;
		context.fill();

		let texture = new THREE.Texture( canvas );
		texture.needsUpdate = true;
		//textureImage = texture.image;

		return texture;
	}

	static generateClassificationTexture(classification){
		let width = 256;
		let height = 256;
		let size = width*height;

		let data = new Uint8Array(4*size);

		for(let x = 0; x < width; x++){
			for(let y = 0; y < height; y++){

				let i = x + width*y;

				let color;
				if(classification[x]){
					color = classification[x];
				}else if(classification[x % 32]){
					color = classification[x % 32];
				}else{
					color = classification.DEFAULT;
				}


				data[4*i+0] = 255 * color.x;
				data[4*i+1] = 255 * color.y;
				data[4*i+2] = 255 * color.z;
				data[4*i+3] = 255 * color.w;
			}
		}

		let texture = new THREE.DataTexture(data, width, height, THREE.RGBAFormat);
		texture.magFilter = THREE.NearestFilter;
		texture.needsUpdate = true;

		return texture;
	}
};



Potree.PointCloudTreeNode = class{

	constructor(){

	}

	getChildren(){
		throw "override function";
	}

	getBoundingBox(){
		throw "override function";
	}

	isLoaded(){
		throw "override function";
	}

	isGeometryNode(){
		throw "override function";
	}

	isTreeNode(){
		throw "override function";
	}

	getLevel(){
		throw "override function";
	}

	getBoundingSphere(){
		throw "override function";
	}

};


Potree.PointCloudTree = class PointCloudTree extends THREE.Object3D{

	constructor(){
		super();

		this.dem = new Potree.DEM(this);
	}

	initialized(){
		return this.root !== null;
	}


};



Potree.PointCloudOctreeGeometryNode.IDCount = 0;

Potree.PointCloudOctreeGeometryNode.prototype = Object.create(Potree.PointCloudTreeNode.prototype);

Potree.PointCloudOctreeGeometryNode.prototype.isGeometryNode = function(){
	return true;
};

Potree.PointCloudOctreeGeometryNode.prototype.getLevel = function(){
	return this.level;
};

Potree.PointCloudOctreeGeometryNode.prototype.isTreeNode = function(){
	return false;
};

Potree.PointCloudOctreeGeometryNode.prototype.isLoaded = function(){
	return this.loaded;
};

Potree.PointCloudOctreeGeometryNode.prototype.getBoundingSphere = function(){
	return this.boundingSphere;
};

Potree.PointCloudOctreeGeometryNode.prototype.getBoundingBox = function(){
	return this.boundingBox;
};

Potree.PointCloudOctreeGeometryNode.prototype.getChildren = function(){
	var children = [];

	for(var i = 0; i < 8; i++){
		if(this.children[i]){
			children.push(this.children[i]);
		}
	}

	return children;
};

Potree.PointCloudOctreeGeometryNode.prototype.getBoundingBox = function(){
	return this.boundingBox;
};

Potree.PointCloudOctreeGeometryNode.prototype.getURL = function(){
	var url = "";

	var version = this.pcoGeometry.loader.version;

	if(version.equalOrHigher("1.5")){
		url = this.pcoGeometry.octreeDir + "/" + this.getHierarchyPath() + "/" + this.name;
	}else if(version.equalOrHigher("1.4")){
		url = this.pcoGeometry.octreeDir + "/" + this.name;
	}else if(version.upTo("1.3")){
		url = this.pcoGeometry.octreeDir + "/" + this.name;
	}

	return url;
};

Potree.PointCloudOctreeGeometryNode.prototype.getHierarchyPath = function(){
	var path = "r/";

	var hierarchyStepSize = this.pcoGeometry.hierarchyStepSize;
	var indices = this.name.substr(1);

	var numParts = Math.floor(indices.length / hierarchyStepSize);
	for(var i = 0; i < numParts; i++){
		path += indices.substr(i * hierarchyStepSize, hierarchyStepSize) + "/";
	}

	path = path.slice(0,-1);

	return path;
};

Potree.PointCloudOctreeGeometryNode.prototype.addChild = function(child){
	this.children[child.index] = child;
	child.parent = this;
};

Potree.PointCloudOctreeGeometryNode.prototype.load = function(){
	if(this.loading === true || this.loaded === true ||this.pcoGeometry.numNodesLoading > 3){
		return;
	}

	this.loading = true;

	this.pcoGeometry.numNodesLoading++;


	if(this.pcoGeometry.loader.version.equalOrHigher("1.5")){
		if((this.level % this.pcoGeometry.hierarchyStepSize) === 0 && this.hasChildren){
			this.loadHierachyThenPoints();
		}else{
			this.loadPoints();
		}
	}else{
		this.loadPoints();
	}


};

Potree.PointCloudOctreeGeometryNode.prototype.loadPoints = function(){
	this.pcoGeometry.loader.load(this);
};


Potree.PointCloudOctreeGeometryNode.prototype.loadHierachyThenPoints = function(){

	var node = this;

	// load hierarchy
	var callback = function(node, hbuffer){
		var count = hbuffer.byteLength / 5;
		var view = new DataView(hbuffer);

		var stack = [];
		var children = view.getUint8(0);
		var numPoints = view.getUint32(1, true);
		node.numPoints = numPoints;
		stack.push({children: children, numPoints: numPoints, name: node.name});

		var decoded = [];

		var offset = 5;
		while(stack.length > 0){

			var snode = stack.shift();
			var mask = 1;
			for(var i = 0; i < 8; i++){
				if((snode.children & mask) !== 0){
					var childIndex = i;
					var childName = snode.name + i;

					var childChildren = view.getUint8(offset);
					var childNumPoints = view.getUint32(offset + 1, true);

					stack.push({children: childChildren, numPoints: childNumPoints, name: childName});

					decoded.push({children: childChildren, numPoints: childNumPoints, name: childName});

					offset += 5;
				}

				mask = mask * 2;
			}

			if(offset === hbuffer.byteLength){
				break;
			}

		}

		//console.log(decoded);

		var nodes = {};
		nodes[node.name] = node;
		var pco = node.pcoGeometry;


		for( var i = 0; i < decoded.length; i++){
			var name = decoded[i].name;
			var numPoints = decoded[i].numPoints;
			var index = parseInt(name.charAt(name.length-1));
			var parentName = name.substring(0, name.length-1);
			var parentNode = nodes[parentName];
			var level = name.length-1;
			var boundingBox = Potree.POCLoader.createChildAABB(parentNode.boundingBox, index);

			var currentNode = new Potree.PointCloudOctreeGeometryNode(name, pco, boundingBox);
			currentNode.level = level;
			currentNode.numPoints = numPoints;
			currentNode.hasChildren = decoded[i].children > 0;
			currentNode.spacing = pco.spacing / Math.pow(2, level);
			parentNode.addChild(currentNode);
			nodes[name] = currentNode;
		}

		node.loadPoints();

	};
	if((node.level % node.pcoGeometry.hierarchyStepSize) === 0){
		//var hurl = node.pcoGeometry.octreeDir + "/../hierarchy/" + node.name + ".hrc";
		var hurl = node.pcoGeometry.octreeDir + "/" + node.getHierarchyPath() + "/" + node.name + ".hrc";

		var xhr = new XMLHttpRequest();
		xhr.open('GET', hurl, true);
		xhr.responseType = 'arraybuffer';
		xhr.overrideMimeType('text/plain; charset=x-user-defined');
		xhr.onreadystatechange = function() {
			if (xhr.readyState === 4) {
				if (xhr.status === 200 || xhr.status === 0) {
					var hbuffer = xhr.response;
					callback(node, hbuffer);
				} else {
					console.log('Failed to load file! HTTP status: ' + xhr.status + ", file: " + url);
				}
			}
		};
		try{
			xhr.send(null);
		}catch(e){
			console.log("fehler beim laden der punktwolke: " + e);
		}
	}

};


Potree.PointCloudOctreeGeometryNode.prototype.getNumPoints = function(){
	return this.numPoints;
};


Potree.PointCloudOctreeGeometryNode.prototype.dispose = function(){
	if(this.geometry && this.parent != null){
		this.geometry.dispose();
		this.geometry = null;
		this.loaded = false;

		//this.dispatchEvent( { type: 'dispose' } );
		for(var i = 0; i < this.oneTimeDisposeHandlers.length; i++){
			var handler = this.oneTimeDisposeHandlers[i];
			handler();
		}
		this.oneTimeDisposeHandlers = [];
	}
};