"use strict"

import * as THREE from 'three/build/three.module.js';
import Stats from 'three/examples/jsm/libs/stats.module.js';
import { OrbitControls } from '../../assets/dtwin3d/three/CustomOrbitControls.js';
import DTUtils from './DTUtils.js';
import CamUtils from './camUtils.js';
import annoUtils from './annoUtils.js';
import raycastUtils from './raycastUtils.js';
import * as TWEENDup from 'three/examples/jsm/libs/tween.module.min.js';
import { PointerLockControls } from '../../assets/dtwin3d/three/CustomFirstPersonControls.js';
import * as PotreeSrc from '../../assets/dtwin3d/potree/build/potree/potree.js';
import { EventDispatcher } from 'three/build/three.module.js';

const TWEEN = TWEENDup.TWEEN;
const Potree = PotreeSrc.Potree;

// ToDo : send Events With DtwinId   

export class Viewer {

    constructor(options) {
        if (options.divId && options.eventHandler) {
            this.dtwinID = options.divId;
            this.canvas = document.querySelector(`#${options.divId}-c`);
            this.eventService = options.eventHandler;
            this.annosEnabled = options.enableAnnotations;
            this.mobileDevice = options.mobileDevice;
            this.assets = {};
            this.pointclouds = [];
            this.prevCameraObj = { 'position': null, 'target': null };
            this.animation = false;
            // this.statsContainer = document.getElementById(`${options.divId}-stats`);
            // this.stats = new Stats();
            // this.statsContainer.appendChild( this.stats.dom );
            this.main();
        } else {
            console.log('Cannot Start Viewer');
        }
    }

    main() {
        const canvas = this.canvas;
        const renderer = this.renderer = new THREE.WebGLRenderer({ canvas });
        renderer.gammaOutput = true;
        const fov = 60;
        const aspect = canvas.clientWidth / canvas.clientHeight;
        const near = 0.1;
        const far = 1000;

        const camera = this.camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
        camera.position.set(0, 10, 20);
        camera.up.set(0, 0, 1);

        const controlsObj = this.controlsObj = {
            'Orbit': new OrbitControls(camera, canvas),
            'Plc': new PointerLockControls(camera, canvas)
        }

        const controls = this.controls = this.controlsObj['Orbit']
        controls.target.set(0, 5, 0);
        controls.update();

        const scene = this.scene = new THREE.Scene();
        scene.background = new THREE.Color('black');

        const light = new THREE.AmbientLight(0x404040);
        light.position.set(0, 0, 0);
        light.intensity = 5;
        scene.add(light);

        // const light1 = new THREE.PointLight(0x404040);
        // camera.add(light1);
        // scene.add(camera);

        this.camUtils = new CamUtils(camera, TWEEN, this.eventService);
        Object.assign(CamUtils.prototype, EventDispatcher.prototype);
        const raycastutils = new raycastUtils(canvas);

        if (this.annosEnabled) {
            this.annoUtils = new annoUtils({
                canvas,
                scene,
                camera,
                mobileDevice: this.mobileDevice,
                eventService: this.eventService,
                raycastUtils: raycastutils
            });
        }

        this.hoverEnabled = true;
        this.clickEnabled = true;

        const that = this;

        window[this.dtwinID] = {
            'camera': camera,
            'scene': scene,
            'controls': controls
        }

        // window['camera'] = camera;
        // window['scene'] = scene;
        // window['controls'] = controls;

        const resizeRendererToDisplaySize = this.resizeRendererToDisplaySize = (renderer) => {
            const canvas = renderer.domElement;
            const width = canvas.clientWidth;
            const height = canvas.clientHeight;
            const needResize = canvas.width !== width || canvas.height !== height;
            if (needResize) {
                renderer.setSize(width, height, false);
            }
            return needResize;
        }

        //custom mousestop event to detect end of a mouse move
        (function (mouseStopDelay) {
            var timeout;
            canvas.addEventListener('mousemove', function (e) {
                clearTimeout(timeout);
                timeout = setTimeout(function () {
                    var event = new CustomEvent("mousestop", {
                        detail: {
                            clientX: e.clientX,
                            clientY: e.clientY
                        },
                        bubbles: true,
                        cancelable: true
                    });
                    e.target.dispatchEvent(event);
                }, mouseStopDelay);
            });
        }(1));

        let renderRequested = false;

        function render() {
            renderRequested = false;
            if (resizeRendererToDisplaySize(renderer)) {
                const canvas = renderer.domElement;
                camera.aspect = canvas.clientWidth / canvas.clientHeight;
                camera.updateProjectionMatrix();
            }
            that.pointclouds && Potree.updatePointClouds(that.pointclouds, camera, renderer);
            that.animation && controls.update();
            renderer.render(scene, camera);
            TWEEN.update();
            // that.stats.update();
            // that.eventService.sendEvent('render',{type: 'camera',id: that.dtwinID, data: camera.clone()});
            // requestAnimationFrame(render); // switching to on-demand rendering to optimize CPU usage
        }

        this.setRendererProperty = function setRendererProperty(data) {
            Object.keys(data).forEach(property => {
                renderer[property] = data[property];
            });
        }

        const requestRenderIfNotRequested = this.requestRenderIfNotRequested = function requestRenderIfNotRequested() {
            if (!renderRequested) {
                renderRequested = true;
                requestAnimationFrame(render);
            }
        }

        function addEventListener() {
            let enablePick = true;
            let touchPick = true;
            let touckNos = 0;
            let timeTrack;
            window.addEventListener('resize', requestRenderIfNotRequested);
            canvas.addEventListener('click', (e) => {
                enablePick && canvasClick(e);
                enablePick = true;
            });
            canvas.addEventListener('touchstart', (e) => {
                touckNos += 1;
                timeTrack = setTimeout( () => {
                    touchPick = false;
                },50);
            });
            canvas.addEventListener('touchend', (e) => {
                if(touchPick && touckNos === 1) {
                    canvasClick(e);
                }
                clearTimeout(timeTrack);
                touchPick = true;
                touckNos = 0;
             });
            canvas.addEventListener('mousestop', (e) => {
                enablePick && canvasHover(e);
            });
            canvas.addEventListener('mousemove', (e) => {
                if(e.which === 1 || e.which === 3) {
                    enablePick = false;
                } else {
                    enablePick = true;
                }
            });
            controlsObj.Orbit.addEventListener('change', () => {
                that.eventService.sendEvent('render',{type: 'camera',id: that.dtwinID, data: camera.clone()});
                // that.eventService.sendEvent('render',{type: 'controls',id: that.dtwinID, data: {maxDistance: controls.maxDistance, target: controls.target}});
                requestRenderIfNotRequested();
            });
            controlsObj.Plc.addEventListener('change', () => {
                that.eventService.sendEvent('render',{type: 'camera',id: that.dtwinID, data: camera.clone()});
                // that.eventService.sendEvent('render',{type: 'controls',id: that.dtwinID, data: {maxDistance: controls.maxDistance, target: controls.target}});
                requestRenderIfNotRequested();
            });
            that.camUtils.addEventListener('render', () => {
                requestRenderIfNotRequested();
            });
        }

        function canvasHover(event) {
            that.eventService.sendEvent('hover', { type: 'hoverout' });
            if (that.hoverEnabled) {
                const pickedObject = raycastutils.pick(scene.children, camera);
                if(pickedObject && pickedObject.onHover) {
                    pickedObject.onHover(pickedObject, event);
                } else if (pickedObject && that.assets.hasOwnProperty(pickedObject.name)) {
                    that.assets[pickedObject.name].onHover(pickedObject, event);
                } else if (pickedObject) {
                    getParent(pickedObject, 'hover')
                        .then((objParent) => {
                            objParent.onHover(pickedObject, event);
                        });
                }
                // hoverPosition = null;
                requestRenderIfNotRequested();
            }
        }

        function canvasClick(event) {
            that.eventService.sendEvent('click', { type: 'clickout' });
            const pickedObject = raycastutils.pick(scene.children, camera);
            if (that.clickEnabled) {
                if(pickedObject && pickedObject.onClick && !that.annoUtils.pickEnabled) {
                    pickedObject.onClick(pickedObject, event);
                } else if (pickedObject && that.assets.hasOwnProperty(pickedObject.name)) {
                    that.assets[pickedObject.name].onClick(pickedObject, event);
                } else if (pickedObject) {
                    getParent(pickedObject, 'click')
                        .then((objParent) => {
                            objParent.onClick(pickedObject, event);
                        });
                }
                requestRenderIfNotRequested();
            } else if(pickedObject && pickedObject.type == 'droneImage' && pickedObject.onClick && !that.annoUtils.pickEnabled) {
                let clickPos = getCursorClickPos(that.canvas, event);
                pickedObject.onClick(clickPos);
            } else if (pickedObject && (pickedObject.renderType && pickedObject.renderType === 'annoTag')&& !that.annoUtils.pickEnabled) {
                pickedObject.onClick(pickedObject, event);
            } else if(pickedObject === null) {
                that.eventService.sendEvent('click', { type: 'space' });
            }
        }

        function getCursorClickPos(canvas, event) {
            const rect = canvas.getBoundingClientRect();
            const x = event.clientX - rect.left;
            const halfRectWidth = rect.width / 2;
            return (x < halfRectWidth) ? 'left' : 'right';
        }

        function getParent(obj, type) {
            if (type == 'hover') {
                return new Promise((resolve, reject) => {
                    obj.traverseAncestors((parent) => {
                        if (parent.onHover) {
                            resolve(parent);
                        } else if (that.assets.hasOwnProperty(parent.name)) {
                            resolve(that.assets[parent.name]);
                        }
                    });
                });
            } else if (type == 'click') {
                return new Promise((resolve, reject) => {
                    obj.traverseAncestors((parent) => {
                        if (parent.onClick) {
                            resolve(parent);
                        } else if (that.assets.hasOwnProperty(parent.name)) {
                            resolve(that.assets[parent.name]);
                        }
                    });
                });
            }
        }

        render();
        addEventListener();

    }

    async addAsset(asset) {
        this.assets[asset.name] = asset;
        return new Promise((resolve, reject) => {
            asset.load().then((objs) => {
                // if (asset.options.transform) {
                //     asset.options.transform = this.genTransMatrix(asset.options.transform);
                // }
                this.assets[asset.name]['objs'] = objs;
                if (Array.isArray(objs)) {
                    objs.forEach((obj) => {
                        this.addAssetRaw(obj);
                    });
                    this.fitCameraToAsset(objs, true, 3000);
                } else if (asset.options.hierarchial) {
                    objs.children.forEach((obj) => {
                        this.addAssetRaw(obj);
                    });
                    this.addAssetRaw(objs.asset);
                    this.fitCameraToAsset(objs.asset, true, 3000);
                } else {
                    this.addAssetRaw(objs);
                    this.fitCameraToAsset(objs, true, 3000);
                }
                asset.onLoad(objs);
                resolve(asset);
            });
        });
    }

    addAssetRaw(obj) {
      if (obj.renderType == 'pointcloud') {
        this.scene.add(obj);
        this.pointclouds.push(obj);
        let self = this;
        let pcRender = function () {
          setTimeout(() => {
            if (!obj.children.length) {
              self.requestRenderIfNotRequested();
              pcRender();
            } else {
              self.requestRenderIfNotRequested();
            }
          }, 1000);
        };
        pcRender();
        // while (!obj.children.length) {
        // console.log('Need To Render')
        // }
      } else {
        this.scene.add(obj);
      }
      this.requestRenderIfNotRequested();
    }

    setPrevCamVals() {
        this.camUtils.setPrevCamVals();
    }

    setCamVals(camObj) {
        this.camUtils.setCamVals(camObj);
    }

    tweenCamPos(inObj, outObj, duration, target) {
        this.camUtils.tweenCamPos(inObj, outObj, duration, target);
    }

    tweenCamPitch(delta) {
        this.camUtils.tweenCamPitch(delta);
    }

    setCamPitch(angle) {
        this.camUtils.setCamPitch(angle);
    }

    tweenCamYaw(delta) {
        this.camUtils.tweenCamYaw(delta);
    }

    syncCamera(camInstc) {
        this.camUtils.syncCamera(camInstc);
    }

    setCamTarget(tarVec) {
        this.camUtils.setCamTarget(tarVec);
    }
    fitCameraToAsset(asset, animate, animationTime=2000) {
        if (animate) {
            this.camUtils.fitCameraToAsset(asset, this.controls, () => {
                this.animate(animationTime);
            });
        } else {
            this.camUtils.fitCameraToAsset(asset, this.controls);
        }
    }

    flyToAsset(assets, animate) {
        if (Array.isArray(assets)) {
            let objArr = assets.map(assetName => {
                let asset = this.assets[assetName];
               if(asset) {
                    let assetObjs = asset.objs;
                    if (asset.options.hierarchial) {
                        return assetObjs.asset
                    } else if (Array.isArray(assetObjs)) {
                        return assetObjs
                    } else {
                        return assetObjs
                    }
               }
            });
            this.fitCameraToAsset(objArr.flat(), animate);
        } else if(typeof(assets) === 'object' && !(assets.isObject3D || assets.isMesh)) {
            let asset = this.assets[assets.asset];
            let childArr = [];
            if(asset) {
                let assetObjs = asset.objs;
                if (asset.options.hierarchial) {
                    let leafAsset = assetObjs.asset;
                    let childObjs = assets.children;
                    childObjs.forEach(objID => {
                        let curChild = leafAsset.children.filter(child => child.name == objID);
                        childArr.push(curChild[0]);
                    });
                } else {
                    let childObjs = assets.children;
                    childObjs.forEach(objID => {
                        let curChild = assetObjs.children.filter(child => child.name == objID);
                        childArr.push(curChild[0]);
                    });
                }
                this.fitCameraToAsset(childArr, animate);
            }
        } else {
            let asset = this.assets[assets];
            if(asset) {
                let assetObjs = asset.objs;
                if (asset.options.hierarchial) {
                    this.fitCameraToAsset(assetObjs.asset, animate);
                } else if (Array.isArray(assetObjs)) {
                    this.fitCameraToAsset(assetObjs, animate);
                } else {
                    this.fitCameraToAsset(assetObjs, animate);
                }
            }
        }
    }

    removeAsset(assetNames) {
        if (Array.isArray(assetNames)) {
            assetNames.map((name) => {
                this.removeAssetRaw(name);
            })
        } else {
            this.removeAssetRaw(assetNames);
        }
    }

    removeAssetRaw(assetName) {
        let asset = this.assets[assetName];
        let assetObjs = asset.objs;
        if (asset.options.hierarchial) {
            this.removeAssetHelper(assetObjs.children);
            this.removeAssetHelper(assetObjs.asset);
            // this.removeAssetHelper(assetObjs.asset);
        } else if (Array.isArray(assetObjs)) {
            this.removeAssetHelper(assetObjs);
        } else {
            this.removeAssetHelper(assetObjs);
        }
    }

    removeAssetHelper(obj) {
        if (Array.isArray(obj)) {
            obj.map(objChild => {
                this.removeAssetHelperRaw(objChild);
            })
        } else {
            this.removeAssetHelperRaw(obj);
        }
    }

    removeAssetHelperRaw(obj3d) {
        obj3d.geometry && obj3d.geometry.dispose();
        obj3d.material && obj3d.material.dispose();
        this.scene.remove(obj3d);
        this.renderer.render(this.scene, this.camera);
    }

    applyStyle(objs, style, recursive = false) {
        if (objs.isObject3D || objs.isMesh) {
            DTUtils.setStyle(objs, style);
        } else {
            let asset = this.assets[objs];
            if(asset) {
                let assetObjs = asset.objs;
                if (asset.options.hierarchial) {
                    if (recursive) {
                        DTUtils.setStyle(assetObjs.children, style);
                        DTUtils.setStyle(assetObjs.asset, style);
                    } else {
                        DTUtils.setStyle(assetObjs.asset, style);
                    }
                } else if (Array.isArray(assetObjs)) {
                    DTUtils.setStyle(assetObjs, style);
                } else {
                    DTUtils.setStyle(assetObjs, style);
                }
            }
        }
    }

    setStyle(objs, style, recursive = false) {
        if (Array.isArray(objs)) {
            objs.map(obj => {
                this.applyStyle(obj, style, recursive);
            })
        } else if (typeof(objs) == 'object' && !(objs.isObject3D || objs.isMesh)) {
            let asset = this.assets[objs.asset];
            if(asset) {
                let assetObjs = asset.objs;
                if (asset.options.hierarchial) {
                    let leafAsset = assetObjs.asset;
                    let childObjs = objs.children;
                    childObjs.forEach(objID => {
                        let curChild = leafAsset.children.filter(child => child.name == objID);
                        DTUtils.setStyle(curChild, style);
                    });
                } else {
                    let childObjs = objs.children;
                    childObjs.forEach(objID => {
                        let curChild = assetObjs.children.filter(child => child.name == objID);
                        DTUtils.setStyle(curChild, style);
                    });
                }
            }
        } else {
            this.applyStyle(objs, style, recursive);
        }
    }

    resetStyle(obj) {
        if (!obj) {
            return;
        }
        obj.material = obj.savedMaterial;
    }

    genTransMatrix(elemets) {
        return new THREE.Matrix4().fromArray(elemets).transpose();
    }

    setNavigationMode(navMode) {
        if (navMode == 'Orbit') {
            if (this.controls.name === 'PointerLockControls') {
                this.setControlsLimits('reset');
                this.controls.unlock();
                this.controls.enabled = false;
                this.controls = this.controlsObj['Orbit'];
                this.controls.enabled = true;
                this.camera.fov = 60;
                this.camera.updateProjectionMatrix();
            }
        } else {
            if (this.controls.name === 'OrbitControls') {
                this.controls.enabled = false;
                this.controls = this.controlsObj['Plc'];
                this.controls.enabled = true;
                this.controls.lock();
            }
        }
        this.controls.dispatchEvent({ type: 'change' });
    }

    syncControls(controlsInst) {
        this.controls.maxDistance = controlsInst.maxDistance;
        this.controls.target.copy(controlsInst.target);
        // this.controls.update();
    }

    setControlsLimits(limits) {
        if(this.controls.name === 'PointerLockControls') {
            if(limits == 'reset') {
                this.controls.resetLimits();
            } else {
                Object.keys(limits).forEach(limit => {
                    this.controls.setLimits(limit, limits[limit]);
                });
            }
        }
    }

    addCtrlClick(subAsset, asset) {
        if (this.assets[subAsset] && this.assets[asset]) {
            let curFn = this.assets[asset].onClick;
            this.assets[asset].onClick = (obj, e) => {
                curFn.apply(this, [obj, e]);
                if (e.ctrlKey) {
                    this.assets[subAsset].onClick(obj.point, e);
                }
            }
        }
    }

    getCamera() {
        return this.camera.clone();
    }

    getAssets() {
        return this.assets;
    }

    dtwinHoverFeature(inCond) {
        this.hoverEnabled = inCond;
    }

    dtwinClickFeature(inCond) {
        this.clickEnabled = inCond;
    }

    addAnnotations(annos, id) {
        this.annoUtils.loadAnnotations(annos, id);
    }

    insertAnnotation(data) {
        this.annoUtils.insertAnnotation(data);
    }

    discardAnnotation() {
        this.annoUtils.discardAnnotation();
    }

    saveAnnotation() {
        this.annoUtils.saveAnnotation();
    }

    removeAnnotations() {
        this.annoUtils.removeAnnotations();
    }

    annosInteraction(condition) {
        this.annoUtils.annosInteraction(condition);
    }

    toggleControls() {
        this.controls.enabled = !this.controls.enabled;
    }

    animate(time) {
        // this.controls.constructor.name === 'OrbitControls' && this.controls.setRotation(true);
        // this.animation = true;
        // setTimeout(() => {
        //     this.controls.constructor.name === 'OrbitControls' && this.controls.setRotation(false);
        //     this.animation = false;
        // }, time);
        this.eventService.sendEvent('animation', {type: 'start'});
        this.controls.setRotation(true);
        this.animation = true;
        this.dtwinHoverFeature(false);
        this.dtwinClickFeature(false);
        setTimeout(() => {
            this.controls.setRotation(false);
            this.animation = false;
            this.dtwinHoverFeature(true);
            this.dtwinClickFeature(true);
            this.eventService.sendEvent('animation', {type: 'stop'});
        }, time);
    }
}


