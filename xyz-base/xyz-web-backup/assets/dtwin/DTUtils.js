"use strict";

import * as THREE from 'three/build/three.module.js';

export default class DTUtils {

  static dumpObject(obj, lines = [], isLast = true, prefix = '') {
    const localPrefix = isLast ? '└─' : '├─';
    lines.push(`${prefix}${prefix ? localPrefix : ''}${obj.name || '*no-name*'} [${obj.type}]`);
    const newPrefix = prefix + (isLast ? '  ' : '│ ');
    const lastNdx = obj.children.length - 1;
    obj.children.forEach((child, ndx) => {
      const isLast = ndx === lastNdx;
      this.dumpObject(child, lines, isLast, newPrefix);
    });
    return lines;
  }

  static setStyleDup(obj, style) {
    if (!obj || !style) {
      return;
    }
    if ((style.color || style.opacity) && !style.material) {

      style.material = new THREE.MeshPhongMaterial();
      if (style.color) {
        style.material.emissive.setHex(style.color);
      }
      if (style.opacity) {
        style.material.transparent = true;
        style.material.opacity = style.opacity;
        style.material.side = THREE.DoubleSide;
      }
    }

    if (typeof style.visibility != 'undefined') {
      obj.visible = style.visibility;
    }
    if (style.material) {
      obj.material = style.material;
    }
  }

  static setStyle(objs, style) {
    if(Array.isArray(objs)) {
      objs.map(obj => {
        // if(obj.renderType) {
          this.setStyleRaw(obj,style)
        // }
      });
    } else {
    //  else if(objs.renderType) {
      this.setStyleRaw(objs,style);
    }
  } 

  static setStyleRaw(obj, style) {
    if (!obj || !style) {
      return;
    }
    if (style.material) {
      obj.material = style.material;
    } else if(style.color != undefined || style.opacity != undefined) {
      let newMat = new THREE.MeshBasicMaterial();
      if (style.color != undefined) {
        newMat.color.set(style.color);
      } else if(obj.material) {
        newMat.color.set(obj.material.color)
      }

      if (style.opacity != undefined) {
        newMat.transparent = true;
        newMat.opacity = style.opacity;
      } else if(obj.material) {
        newMat.transparent = obj.material.transparent;
        newMat.opacity = obj.material.opacity;
      }
      obj.material = newMat;
    }
    if(style.visibility != undefined) {
      if(style.visibility){
        if(obj.renderType) {
            obj.visible = style.visibility;
          }
        } else {
          obj.visible = style.visibility;
        }
    }
  }
}