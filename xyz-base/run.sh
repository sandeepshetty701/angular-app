#!/bin/bash

cd /srv
npm install --silent
exec pm2 --no-daemon start app.js
