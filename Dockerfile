FROM node:10-alpine

# WORKDIR specifies the application directory
WORKDIR /app

# Copying package.json file to the app directory
COPY xyz-base/package.json /app

# Installing npm for DOCKER
RUN npm install

# Copying rest of the application to app directory
COPY . /app

# Starting the application using npm start
CMD ["npm","start"]